package tradingbot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tradingbot.analyzer.PumpSignalAnalyzer;
import tradingbot.analyzer.TAAnalyzer;
import tradingbot.model.PumpSignal;
import tradingbot.service.PumpService;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Controller
public class PumpController {

  @Autowired
  PumpService pumpService;

  @Autowired
  TAAnalyzer taAnalyzer;

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "api/analyze/{symbol}/{time}",method = RequestMethod.POST,produces = "text/plain")
  @ResponseBody
  public ResponseEntity<String> createPumpAnalyze(@PathVariable String symbol, @PathVariable long time) {
    try {
      if(pumpService.isPumpSignalExisted(symbol, time)) {
        return new ResponseEntity<>("created",HttpStatus.OK);
      }
      PumpSignal pumpSignal = pumpService.getPumpSignalBySymbolAndTime(symbol.toUpperCase(), time);
      PumpSignalAnalyzer pumpSignalAnalyzer = new PumpSignalAnalyzer(pumpSignal);
      pumpSignalAnalyzer.buildMa5();
      pumpSignalAnalyzer.buildMa9();
      pumpSignalAnalyzer.buildMa13();
      pumpSignalAnalyzer.buildMa20();
      pumpSignalAnalyzer.buildMa50();
      pumpSignalAnalyzer.buildMa100();
      pumpSignalAnalyzer.buildRSI();
      pumpSignalAnalyzer.buildStochOsc();
      pumpSignalAnalyzer.buildHistogram();
      pumpSignalAnalyzer.buildRecentClosePrice();
      pumpSignalAnalyzer.buildRecentTradeAndBuySellVolume();
      pumpSignalAnalyzer.buildFuturePrices();
      pumpService.savePumpSignals(pumpSignal);
      return new ResponseEntity<>("created",HttpStatus.OK);
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Something is wrong,please try later",HttpStatus.BAD_REQUEST);
  }

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "api/analyze/{symbol}/{time}",method = RequestMethod.PUT,produces = "text/plain")
  @ResponseBody
  public ResponseEntity<String> updatePumpAnalyze(@PathVariable String symbol, @PathVariable long time) {
    try {
      PumpSignal pumpSignal = pumpService.getPumpSignalBySymbolAndTime(symbol.toUpperCase(), time);
      PumpSignalAnalyzer pumpSignalAnalyzer = new PumpSignalAnalyzer(pumpSignal);
      pumpSignalAnalyzer.buildMa5();
      pumpSignalAnalyzer.buildMa9();
      pumpSignalAnalyzer.buildMa13();
      pumpSignalAnalyzer.buildMa20();
      pumpSignalAnalyzer.buildMa50();
      pumpSignalAnalyzer.buildMa100();
      pumpSignalAnalyzer.buildRSI();
      pumpSignalAnalyzer.buildStochOsc();
      pumpSignalAnalyzer.buildHistogram();
      pumpSignalAnalyzer.buildRecentClosePrice();
      pumpSignalAnalyzer.buildRecentTradeAndBuySellVolume();
      pumpSignalAnalyzer.buildFuturePrices();
      pumpService.updatePumpSignals(pumpSignal);
      return new ResponseEntity<>("updated",HttpStatus.OK);
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Something is wrong,please try later",HttpStatus.BAD_REQUEST);
  }

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "api/analyze/btctrend",method = RequestMethod.GET,produces = "application/json")
  @ResponseBody
  public Map<String,List<Float>> getCurrentBTCTrendMA5() {
    return taAnalyzer.getBtcTrendCache();
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(value = "api/analyze/btcsignals",method = RequestMethod.GET,produces = "application/json")
  @ResponseBody
  public Map<String,List<String>> getCurrentBTCSignals() {
    return taAnalyzer.getBtcSignalsCache();
  }
}
