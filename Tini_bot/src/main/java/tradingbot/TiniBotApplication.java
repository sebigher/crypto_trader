package tradingbot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import tradingbot.analyzer.TAAnalyzer;

/**
 * Created by truongnhukhang on 4/27/18.
 */
@SpringBootApplication
@EnableAsync
@EnableScheduling
public class TiniBotApplication implements CommandLineRunner {
  @Autowired
  TAAnalyzer taAnalyzer;

  public static void main(String[] args) {
    SpringApplication springApplication =
        new SpringApplicationBuilder()
            .sources(TiniBotApplication.class)
            .build();

    springApplication.run(TiniBotApplication.class, args);
  }

  @Override
  public void run(String... strings) throws Exception {
    taAnalyzer.updateBTCTrendCache();
    taAnalyzer.updateBtcSignalsCache();
//    PumpService pumpService = new PumpService();
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//    String strDate = "2018-05-03";
//    List<PumpSignal> pumpSignals = pumpService.getPumpSignals(sdf.parse(strDate).getTime(), 10);
//    BlockingQueue<PumpSignal> pumpSignalBlockingQueue = new LinkedBlockingQueue<>();
//    pumpSignals.forEach(pumpSignal -> {
//      try {
//        pumpSignalBlockingQueue.put(pumpSignal);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//
//    });
//    while (!pumpSignalBlockingQueue.isEmpty()) {
//      PumpSignal pumpSignal = null;
//      try {
//        pumpSignal = pumpSignalBlockingQueue.poll();
//        PumpSignalAnalyzer pumpSignalAnalyzer = new PumpSignalAnalyzer(pumpSignal);
//        pumpSignalAnalyzer.buildMa20();
//        pumpSignalAnalyzer.buildMa50();
//        pumpSignalAnalyzer.buildMa100();
//        pumpSignalAnalyzer.buildRSI();
//        pumpSignalAnalyzer.buildStochRSI();
//        pumpSignalAnalyzer.buildHistogram();
//        pumpSignalAnalyzer.buildRecentVolume();
//        pumpSignalAnalyzer.buildRecentClosePrice();
//        pumpSignalAnalyzer.buildRecentTradeAndBuySellVolume();
//        pumpSignalAnalyzer.buildFuturePrices();
//        pumpService.savePumpSignals(pumpSignal);
//      } catch (Exception e) {
//        System.out.println("Error : " + pumpSignal.getSymbol() + "-" + pumpSignal.getDateTime() + " "+ e.getMessage());
//        pumpSignalBlockingQueue.put(pumpSignal);
//      }
//    PumpSignal pumpSignal = pumpService.getPumpSignalById("7wUxf02uknfSpczQpDTI");
//    PumpSignalAnalyzer pumpSignalAnalyzer = new PumpSignalAnalyzer(pumpSignal);
//    pumpSignalAnalyzer.buildMa5();
//    pumpSignalAnalyzer.buildMa9();
//    pumpSignalAnalyzer.buildMa13();
//    pumpSignalAnalyzer.buildMa20();
//    pumpSignalAnalyzer.buildMa50();
//    pumpSignalAnalyzer.buildMa100();
//    pumpSignalAnalyzer.buildRSI();
//    pumpSignalAnalyzer.buildStochOsc();
//    pumpSignalAnalyzer.buildHistogram();
//    pumpSignalAnalyzer.buildRecentClosePrice();
//    pumpSignalAnalyzer.buildRecentTradeAndBuySellVolume();
//    pumpSignalAnalyzer.buildFuturePrices();
//    pumpService.savePumpSignals(pumpSignal);
    System.out.println("Start Tini bot");

  }

}
