package tradingbot.analyzer;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.ta4j.core.*;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import tradingbot.indicator.MACDHistogramIndicator;
import tradingbot.model.Macd;
import tradingbot.model.PumpSignal;
import tradingbot.model.StochOscillator;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PumpSignalAnalyzer {

  private PumpSignal pumpSignal;
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient client = factory.newRestClient();
  TimeSeries timeFrame_1min = new BaseTimeSeries("1");
  TimeSeries timeFrame_5min = new BaseTimeSeries("5");
  TimeSeries timeFrame_15min = new BaseTimeSeries("15");
  TimeSeries timeFrame_30min = new BaseTimeSeries("30");
  TimeSeries timeFrame_60min = new BaseTimeSeries("60");
  TimeSeries timeFrame_120min = new BaseTimeSeries("120");
  TimeSeries timeFrame_240min = new BaseTimeSeries("240");
  TimeSeries timeFrame_1440min = new BaseTimeSeries("1440");
  DecimalFormat df = new DecimalFormat("#.######");

  public PumpSignalAnalyzer(PumpSignal pumpSignal) {
    this.pumpSignal = pumpSignal;
    buildTimeSeries(pumpSignal, CandlestickInterval.ONE_MINUTE, timeFrame_1min);
    buildTimeSeries(pumpSignal, CandlestickInterval.FIVE_MINUTES, timeFrame_5min);
    buildTimeSeries(pumpSignal, CandlestickInterval.FIFTEEN_MINUTES, timeFrame_15min);
    buildTimeSeries(pumpSignal, CandlestickInterval.HALF_HOURLY, timeFrame_30min);
    buildTimeSeries(pumpSignal, CandlestickInterval.HOURLY, timeFrame_60min);
    buildTimeSeries(pumpSignal, CandlestickInterval.TWO_HOURLY, timeFrame_120min);
    buildTimeSeries(pumpSignal, CandlestickInterval.FOUR_HOURLY, timeFrame_240min);
    buildTimeSeries(pumpSignal, CandlestickInterval.DAILY, timeFrame_1440min);
  }

  private void buildTimeSeries(PumpSignal pumpSignal, CandlestickInterval interval, TimeSeries timeSeries) {
    client.getCandlestickBars(pumpSignal.getSymbol(), interval, 120, null, pumpSignal.getDateTime())
        .stream().map(candlestick -> {
      return convertToBar(candlestick, 1);

    }).forEach(timeSeries::addBar);
  }

  public void buildMa5() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 5;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa5TimeFrames(sma);

  }

  public void buildMa9() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 9;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa9TimeFrames(sma);

  }

  public void buildMa13() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 13;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa13TimeFrames(sma);
  }

  public void buildMa20() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 20;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa20TimeFrames(sma);
  }


  public void buildRecentClosePrice() {
    Map<String, List<Candlestick>> recentCloseBar = new TreeMap<>();
    recentCloseBar.put("1", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("5", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.FIVE_MINUTES, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("15", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.FIFTEEN_MINUTES, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("30", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.HALF_HOURLY, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("60", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.HOURLY, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("120", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.TWO_HOURLY, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("240", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.FOUR_HOURLY, 50, null, pumpSignal.getDateTime()));
    recentCloseBar.put("1440", client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.DAILY, 50, null, pumpSignal.getDateTime()));
    pumpSignal.setCloseTimeFrames(recentCloseBar);
  }

  private Bar convertToBar(Candlestick candlestick, Integer duration) {
    Bar bar = new BaseBar(Duration.ofMinutes(duration),
        Instant.ofEpochMilli(candlestick.getOpenTime()).atZone(ZoneOffset.UTC),
        Decimal.valueOf(candlestick.getOpen()),
        Decimal.valueOf(candlestick.getHigh()),
        Decimal.valueOf(candlestick.getLow()),
        Decimal.valueOf(candlestick.getClose()),
        Decimal.valueOf(candlestick.getVolume()));
    return bar;
  }


  private void generateSMA(TimeSeries series, String timeInterval, Map<String, List<String>> ma20, int timeFrame) {
    ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
    SMAIndicator smaIndicator = new SMAIndicator(closePrice, timeFrame);
    int endExclusive = series.getEndIndex() > 50 ? 50 : series.getEndIndex();
    ma20.put(timeInterval, IntStream.range(0, endExclusive).boxed().map(integer -> smaIndicator.getValue(series.getEndIndex() - integer).toString()).collect(Collectors.toList()));
  }

  private void generateRecentRSI(TimeSeries series, String timeInterval, Map<String, List<String>> rsi, int timeFrame) {
    ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
    RSIIndicator rsiIndicator = new RSIIndicator(closePrice, timeFrame);
    int endExclusive = series.getEndIndex() > 50 ? 50 : series.getEndIndex();
    List<String> recentRsi = IntStream.range(0, endExclusive).boxed().map(integer -> df.format(rsiIndicator.getValue(series.getEndIndex() - integer))).collect(Collectors.toList());
    rsi.put(timeInterval, recentRsi);
  }

  private void generateRecentStochOscillator(TimeSeries series, String timeInterval, Map<String,List<StochOscillator>> stochOsc) {
    StochasticOscillatorKIndicator oscillatorKIndicator = new StochasticOscillatorKIndicator(series,14);
    StochasticOscillatorDIndicator oscillatorDIndicator = new StochasticOscillatorDIndicator(oscillatorKIndicator);
    int endExclusive = series.getEndIndex() > 50 ? 50 : series.getEndIndex();
    List<StochOscillator> recentStoch = IntStream.range(0, endExclusive).boxed().map(integer -> {
      StochOscillator stochOscillator = new StochOscillator();
      stochOscillator.setkLine( df.format(oscillatorKIndicator.getValue(integer)));
      stochOscillator.setdLine( df.format(oscillatorDIndicator.getValue(integer)));
      return stochOscillator;
    }).collect(Collectors.toList());
    stochOsc.put(timeInterval,recentStoch);
  }

  private void generateRecentStochRSI(TimeSeries series, String timeInterval, Map<String, List<String>> stochRsi, int timeFrame) {
    ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
    StochasticRSIIndicator stochasticRSIIndicator = new StochasticRSIIndicator(closePrice, timeFrame);
    int endExclusive = series.getEndIndex() > 50 ? 50 : series.getEndIndex();
    List<String> recentStochRsi = IntStream.range(0, endExclusive).boxed().map(integer -> df.format(stochasticRSIIndicator.getValue(series.getEndIndex() - integer))).collect(Collectors.toList());
    stochRsi.put(timeInterval, recentStochRsi);
  }

  private void generateHistogram(TimeSeries series, String timeInterval, Map<String, List<Macd>> histogram) {
    ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
    MACDIndicator macdIndicator = new MACDIndicator(closePrice);
    EMAIndicator signallineIndicator = new EMAIndicator(macdIndicator, 9);
    int endExclusive = series.getEndIndex() > 50 ? 50 : series.getEndIndex();
    MACDHistogramIndicator macdHistogramIndicator = new MACDHistogramIndicator(closePrice, macdIndicator);
    List<Macd> recentHistogram = IntStream.range(0, endExclusive).boxed().map(integer -> {
      Macd macd = new Macd();
      macd.setMacdLine(macdIndicator.getValue(series.getEndIndex() - integer).toString());
      macd.setSignalLine(signallineIndicator.getValue(series.getEndIndex() - integer).toString());
      macd.setHistogram(macdHistogramIndicator.getValue(series.getEndIndex() - integer).toString());
      return macd;
    }).collect(Collectors.toList());
    histogram.put(timeInterval, recentHistogram);
  }

  public void buildMa50() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 50;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa50TimeFrames(sma);

  }

  public void buildMa100() {
    Map<String, List<String>> sma = new TreeMap<>();
    int timeFrame = 100;
    generateSMA(timeFrame_1min, "1", sma, timeFrame);
    generateSMA(timeFrame_5min, "5", sma, timeFrame);
    generateSMA(timeFrame_15min, "15", sma, timeFrame);
    generateSMA(timeFrame_30min, "30", sma, timeFrame);
    generateSMA(timeFrame_60min, "60", sma, timeFrame);
    generateSMA(timeFrame_120min, "120", sma, timeFrame);
    generateSMA(timeFrame_240min, "240", sma, timeFrame);
    generateSMA(timeFrame_1440min, "1440", sma, timeFrame);
    pumpSignal.setMa100TimeFrames(sma);
  }

  public void buildRSI() {
    Map<String, List<String>> rsiTimeFrame = new TreeMap<>();
    generateRecentRSI(timeFrame_1min, "1", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_5min, "5", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_15min, "15", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_30min, "30", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_60min, "60", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_120min, "120", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_240min, "240", rsiTimeFrame, 14);
    generateRecentRSI(timeFrame_1440min, "1440", rsiTimeFrame, 14);
    pumpSignal.setRsiTimeFrames(rsiTimeFrame);

  }

  public void buildStochOsc() {
    Map<String, List<StochOscillator>> stochOscTimeFrame = new TreeMap<>();
    generateRecentStochOscillator(timeFrame_1min, "1", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_5min, "5", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_15min, "15", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_30min, "30", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_60min, "60", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_120min, "120", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_240min, "240", stochOscTimeFrame);
    generateRecentStochOscillator(timeFrame_1440min, "1440", stochOscTimeFrame);
    pumpSignal.setStochOscTimeFrames(stochOscTimeFrame);

  }

  public void buildStochRSI() {
    Map<String, List<String>> stochRsiTimeFrame = new TreeMap<>();
    generateRecentStochRSI(timeFrame_1min, "1", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_5min, "5", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_15min, "15", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_30min, "30", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_60min, "60", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_120min, "120", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_240min, "240", stochRsiTimeFrame, 14);
    generateRecentStochRSI(timeFrame_1440min, "1440", stochRsiTimeFrame, 14);
    pumpSignal.setStochRsiTimeFrames(stochRsiTimeFrame);

  }

  public void buildHistogram() {
    Map<String, List<Macd>> histogramTimeFrame = new TreeMap<>();
    generateHistogram(timeFrame_1min, "1", histogramTimeFrame);
    generateHistogram(timeFrame_5min, "5", histogramTimeFrame);
    generateHistogram(timeFrame_15min, "15", histogramTimeFrame);
    generateHistogram(timeFrame_30min, "30", histogramTimeFrame);
    generateHistogram(timeFrame_60min, "60", histogramTimeFrame);
    generateHistogram(timeFrame_120min, "120", histogramTimeFrame);
    generateHistogram(timeFrame_240min, "240", histogramTimeFrame);
    generateHistogram(timeFrame_1440min, "1440", histogramTimeFrame);
    pumpSignal.setHistogramTimeFrames(histogramTimeFrame);
  }

  public void buildRecentTradeAndBuySellVolume() {
    List<Map<String, String>> recentTrades = client.getAggTrades(pumpSignal.getSymbol(), null, null, pumpSignal.getDateTime() - (60 * 60000), pumpSignal.getDateTime())
        .stream().map(aggTrade -> {
          Map<String, String> tradeEvent = new HashMap<>();
          tradeEvent.put("Type", aggTrade.isBuyerMaker() ? "S" : "B");
          tradeEvent.put("Quantity", aggTrade.getQuantity());
          tradeEvent.put("Time", String.valueOf(aggTrade.getTradeTime()));
          tradeEvent.put("Price", aggTrade.getPrice());
          return tradeEvent;
        }).collect(Collectors.toList());
//        pumpSignal.setRecentTrades(recentTrades);
    List<Map<String, String>> buyTrades = recentTrades.stream().filter(tradeEvent -> tradeEvent.get("Type").equals("B")).collect(Collectors.toList());
    List<Map<String, String>> sellTrades = recentTrades.stream().filter(tradeEvent -> tradeEvent.get("Type").equals("S")).collect(Collectors.toList());
    double buy_30min_Volume = buildBuySellVolume(buyTrades, 30, pumpSignal.getDateTime());
    double buy_30min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 30, pumpSignal.getDateTime());
    double sell_30min_Volume = buildBuySellVolume(sellTrades, 30, pumpSignal.getDateTime());
    double sell_30min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 30, pumpSignal.getDateTime());
    double buy_60min_Volume = buildBuySellVolume(buyTrades, 60, pumpSignal.getDateTime());
    double buy_60min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 60, pumpSignal.getDateTime());
    double sell_60min_Volume = buildBuySellVolume(sellTrades, 60, pumpSignal.getDateTime());
    double sell_60min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 60, pumpSignal.getDateTime());
    double buy_15min_Volume = buildBuySellVolume(buyTrades, 15, pumpSignal.getDateTime());
    double buy_15min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 15, pumpSignal.getDateTime());
    double sell_15min_Volume = buildBuySellVolume(sellTrades, 15, pumpSignal.getDateTime());
    double sell_15min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 15, pumpSignal.getDateTime());

    Map<String, Double> buyVolume = new TreeMap<>();
    Map<String, Double> sellVolume = new TreeMap<>();
    buyVolume.put("15", buy_15min_Volume);
    buyVolume.put("30", buy_30min_Volume);
    buyVolume.put("60", buy_60min_Volume);
    sellVolume.put("15", sell_15min_Volume);
    sellVolume.put("30", sell_30min_Volume);
    sellVolume.put("60", sell_60min_Volume);
    pumpSignal.setRecentBuyVolume(buyVolume);
    pumpSignal.setRecentSellVolume(sellVolume);
    Map<String, Double> buySymbolVolume = new TreeMap<>();
    Map<String, Double> sellSymbolVolume = new TreeMap<>();
    buySymbolVolume.put("15", buy_15min_Volume_Symbol);
    buySymbolVolume.put("30", buy_30min_Volume_Symbol);
    buySymbolVolume.put("60", buy_60min_Volume_Symbol);
    sellSymbolVolume.put("15", sell_15min_Volume_Symbol);
    sellSymbolVolume.put("30", sell_30min_Volume_Symbol);
    sellSymbolVolume.put("60", sell_60min_Volume_Symbol);
    pumpSignal.setRecentBuySymbolVolume(buySymbolVolume);
    pumpSignal.setRecentSellSymbolVolume(sellSymbolVolume);
  }

  public double buildBuySellVolume(List<Map<String, String>> trades, int period, long latestTime) {
    double volume = 0;
    long startTime = 0;
    if (period != -1) {
      startTime = latestTime - (period * 60000);
    }
    long finalStartTime = startTime;
    trades = trades.stream().filter(trade -> Long.valueOf(trade.get("Time")) > finalStartTime).collect(Collectors.toList());
    for (int i = 0; i < trades.size(); i++) {
      Map<String, String> buyEvent = trades.get(i);
      volume = volume + Decimal.valueOf(buyEvent.get("Quantity")).multipliedBy(Decimal.valueOf(buyEvent.get("Price"))).doubleValue();
    }
    return volume;
  }

  public double buildBuySellVolumeSymbol(List<Map<String, String>> trades, int period, long latestTime) {
    double volume = 0;
    long startTime = 0;
    if (period != -1) {
      startTime = latestTime - (period * 60000);
    }
    long finalStartTime = startTime;
    trades = trades.stream().filter(trade -> Long.valueOf(trade.get("Time")) > finalStartTime).collect(Collectors.toList());
    for (int i = 0; i < trades.size(); i++) {
      Map<String, String> buyEvent = trades.get(i);
      volume = volume + Decimal.valueOf(buyEvent.get("Quantity")).doubleValue();
    }
    return volume;
  }

  public void buildFuturePrices() {
    List<Candlestick> after3Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 3));
    List<Candlestick> after5Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 5));
    List<Candlestick> after15Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 15));
    List<Candlestick> after30Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 30));
    List<Candlestick> after60Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 60));
    List<Candlestick> after240Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 240));
    List<Candlestick> after1440Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 1440));
    List<Candlestick> after2880Min = client.getCandlestickBars(pumpSignal.getSymbol(), CandlestickInterval.ONE_MINUTE, 1, null, pumpSignal.getDateTime() + (60000 * 2880));
    Map<String, String> futurePrice = new TreeMap<>();
    futurePrice.put("2", after3Min.get(0).getClose());
    futurePrice.put("5", after5Min.get(0).getClose());
    futurePrice.put("15", after15Min.get(0).getClose());
    futurePrice.put("30", after30Min.get(0).getClose());
    futurePrice.put("60", after60Min.get(0).getClose());
    futurePrice.put("240", after240Min.get(0).getClose());
    futurePrice.put("1440", after1440Min.get(0).getClose());
    futurePrice.put("2880", after2880Min.get(0).getClose());
    pumpSignal.setFuturePrices(futurePrice);
  }
}
