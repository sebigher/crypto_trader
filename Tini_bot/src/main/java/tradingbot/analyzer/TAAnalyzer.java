package tradingbot.analyzer;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import tradingbot.TAWorker;
import tradingbot.model.CandleBar;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TAAnalyzer {
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient client = factory.newRestClient();
  Map<String,List<Float>> btcTrendCache = new ConcurrentHashMap<>();
  Map<String,List<String>> btcSignalsCache = new ConcurrentHashMap<>();

  @Scheduled(fixedDelay = 300000 )
  public void updateBTCTrendCache() {
    List<Candlestick> candlesticks4h = client.getCandlestickBars("BTCUSDT",CandlestickInterval.FOUR_HOURLY,19,null,System.currentTimeMillis());
    TimeSeries timeSeries4h = new BaseTimeSeries();
    candlesticks4h.forEach(candlestick -> {
      CandleBar bar = new CandleBar(Duration.ofHours(4),
          Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
          Decimal.valueOf(candlestick.getOpen()),
          Decimal.valueOf(candlestick.getHigh()),
          Decimal.valueOf(candlestick.getLow()),
          Decimal.valueOf(candlestick.getClose()),
          Decimal.valueOf(candlestick.getVolume()));
      try {
        timeSeries4h.addBar(bar);
      } catch (IllegalArgumentException e) {
        System.out.println(timeSeries4h.getLastBar().getBeginTime() + " _ " + timeSeries4h.getLastBar().getEndTime());
        System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
      }
    });
    SMAIndicator ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries4h), 5);
    List<Float> ma5Value = new ArrayList<>();
    for (int i = 5; i < timeSeries4h.getEndIndex(); i++) {
      ma5Value.add(ma5Indicator.getValue(i).floatValue());
    }
    btcTrendCache.put("240",ma5Value);
    List<Candlestick> candlesticks1d = client.getCandlestickBars("BTCUSDT",CandlestickInterval.DAILY,19,null,System.currentTimeMillis());
    TimeSeries timeSeries1d = new BaseTimeSeries();
    candlesticks1d.forEach(candlestick -> {
      CandleBar bar = new CandleBar(Duration.ofHours(4),
          Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
          Decimal.valueOf(candlestick.getOpen()),
          Decimal.valueOf(candlestick.getHigh()),
          Decimal.valueOf(candlestick.getLow()),
          Decimal.valueOf(candlestick.getClose()),
          Decimal.valueOf(candlestick.getVolume()));
      try {
        timeSeries1d.addBar(bar);
      } catch (IllegalArgumentException e) {
        System.out.println(timeSeries1d.getLastBar().getBeginTime() + " _ " + timeSeries1d.getLastBar().getEndTime());
        System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
      }
    });
    ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries1d), 5);
    List<Float>  ma5Value1d = new ArrayList<>();
    for (int i = 5; i < timeSeries1d.getEndIndex(); i++) {
      ma5Value1d.add(ma5Indicator.getValue(i).floatValue());
    }
    btcTrendCache.put("1440",ma5Value1d);
  }

  @Scheduled(fixedDelay = 300000 )
  public void updateBtcSignalsCache() {
    List<Candlestick> candlesticks_30min = client.getCandlestickBars("BTCUSDT", CandlestickInterval.HALF_HOURLY, 100, null, System.currentTimeMillis());
    TimeSeries timeSeries_30min = new BaseTimeSeries();
    buildTimeSeries(candlesticks_30min, timeSeries_30min, Duration.ofMinutes(30));
    TAWorker taWorker_30 = new TAWorker(timeSeries_30min);
    btcSignalsCache.put("30",taWorker_30.buildSignalListFromSeriesIndex(timeSeries_30min.getEndIndex()));
    
    List<Candlestick> candlesticks_60min = client.getCandlestickBars("BTCUSDT", CandlestickInterval.HOURLY, 100, null, System.currentTimeMillis());
    TimeSeries timeSeries_60min = new BaseTimeSeries();
    buildTimeSeries(candlesticks_60min, timeSeries_60min, Duration.ofMinutes(60));
    TAWorker taWorker_60 = new TAWorker(timeSeries_60min);
    btcSignalsCache.put("60",taWorker_60.buildSignalListFromSeriesIndex(timeSeries_60min.getEndIndex()));
    
    List<Candlestick> candlesticks_120min = client.getCandlestickBars("BTCUSDT", CandlestickInterval.TWO_HOURLY, 100, null, System.currentTimeMillis());
    TimeSeries timeSeries_120min = new BaseTimeSeries();
    buildTimeSeries(candlesticks_120min, timeSeries_120min, Duration.ofMinutes(120));
    TAWorker taWorker_120 = new TAWorker(timeSeries_120min);
    btcSignalsCache.put("120",taWorker_120.buildSignalListFromSeriesIndex(timeSeries_120min.getEndIndex()));
    
    List<Candlestick> candlesticks_240min = client.getCandlestickBars("BTCUSDT", CandlestickInterval.FOUR_HOURLY, 100, null, System.currentTimeMillis());
    TimeSeries timeSeries_240min = new BaseTimeSeries();
    buildTimeSeries(candlesticks_240min, timeSeries_240min, Duration.ofMinutes(240));
    TAWorker taWorker_240 = new TAWorker(timeSeries_240min);
    btcSignalsCache.put("240",taWorker_240.buildSignalListFromSeriesIndex(timeSeries_240min.getEndIndex()));
    
    List<Candlestick> candlesticks_1440min = client.getCandlestickBars("BTCUSDT", CandlestickInterval.DAILY, 100, null, System.currentTimeMillis());
    TimeSeries timeSeries_1440min = new BaseTimeSeries();
    buildTimeSeries(candlesticks_1440min, timeSeries_1440min, Duration.ofMinutes(1440));
    TAWorker taWorker_1440 = new TAWorker(timeSeries_1440min);
    btcSignalsCache.put("1440",taWorker_1440.buildSignalListFromSeriesIndex(timeSeries_1440min.getEndIndex()));

  }

  private void buildTimeSeries(List<Candlestick> candlesticks, TimeSeries timeSeries_30min, Duration duration) {
    candlesticks.forEach(candlestick -> {
      CandleBar bar = new CandleBar(duration,
          Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
          Decimal.valueOf(candlestick.getOpen()),
          Decimal.valueOf(candlestick.getHigh()),
          Decimal.valueOf(candlestick.getLow()),
          Decimal.valueOf(candlestick.getClose()),
          Decimal.valueOf(candlestick.getVolume()));
      try {

        timeSeries_30min.addBar(bar);
      } catch (IllegalArgumentException e) {
        System.out.println(timeSeries_30min.getLastBar().getBeginTime() + " _ " + timeSeries_30min.getLastBar().getEndTime());
        System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
      }
    });
  }

  public Map<String, List<Float>> getBtcTrendCache() {
    return btcTrendCache;
  }

  public Map<String, List<String>> getBtcSignalsCache() {
    return btcSignalsCache;
  }

  public void setBtcSignalsCache(Map<String, List<String>> btcSignalsCache) {
    this.btcSignalsCache = btcSignalsCache;
  }
}
