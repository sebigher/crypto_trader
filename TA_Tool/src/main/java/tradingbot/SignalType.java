package tradingbot;

/**
 * Created by truongnhukhang on 6/1/18.
 */
public enum SignalType {
  CANDLE_IN_MA5("Candle in MA5",2),
  CANDLE_IN_MA9("Candle in MA9",3),
  CANDLE_IN_MA20("Candle in MA20",4),
  CANDLE_ABOVE_MA5("Candle above MA5",3),
  CANDLE_ABOVE_MA9("Candle above MA9",4),
  CANDLE_ABOVE_MA20("Candle above MA20",5),
  MA5_AlMOST_MA9("MA5 ALMOST_CROSS MA9",2),
  MA5_AlMOST_MA20("MA5 ALMOST_CROSS MA20",3),
  MA9_AlMOST_MA20("MA9 ALMOST_CROSS MA20",4),
  MA5_CROSS_MA9("MA5 CROSS MA9",2),
  MA5_CROSS_MA20("MA5 CROSS MA20",3),
  MA9_CROSS_MA20("MA9 CROSS MA20",4),
  RSI_BULLISH_DIVERGENCE("Rsi Bull divergence",2),
  RSI_UNDER_30("RSI under 30",2),
  THREE_BULLISH_CANDLE("Three Bull candle",1),
  MACD_CROSS("MACD Bull cross",2),
  MACD_HISTOGRAM_BULLISH_DIVERGENCE("Histogram Bull divergence",1),
  MACDLINE_BULLISH_DIVERGENCE("MACDLine Bull divergence",1),
  VOLUME_GREATER_MA50("Volume greater MA50",5)
  ;
  String signalName;
  Integer score;
  SignalType(String signalName) {
    this.signalName = signalName;
  }

  SignalType(String signalName, Integer score) {
    this.signalName = signalName;
    this.score = score;
  }

  public Integer getScore() {
    return score;
  }

  @Override
  public String toString() {
    return signalName;
  }
}
