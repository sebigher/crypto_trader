package tradingbot.rule;

import org.ta4j.core.Bar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class RSIBullishDivergence extends AbstractRule {

  public RSIBullishDivergence(RSIIndicator rsiIndicator) {
    this.rsiIndicator = rsiIndicator;
  }

  RSIIndicator rsiIndicator;

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal currentRSI = rsiIndicator.getValue(index);
    Decimal previousRSI = rsiIndicator.getValue(index-1);
    Bar theLowerCandle = null;
    if(previousRSI.isGreaterThan(currentRSI)) {
      for (int i = 2; i < 15; i++) {
        Decimal value = rsiIndicator.getValue(index-i);
        if(value.isLessThanOrEqual(currentRSI) && value.isLessThan(rsiIndicator.getValue(index-i-1)) && value.isLessThan(rsiIndicator.getValue(index-i+1))) {
          theLowerCandle = rsiIndicator.getTimeSeries().getBar(index-i);
          break;
        }
      }
    }
    if(theLowerCandle==null) {
      return false;
    }
    return theLowerCandle.getClosePrice().isGreaterThanOrEqual(rsiIndicator.getTimeSeries().getBar(index).getClosePrice());
  }
}
