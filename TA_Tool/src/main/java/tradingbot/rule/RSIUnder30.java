package tradingbot.rule;

import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class RSIUnder30  extends AbstractRule {

  RSIIndicator rsiIndicator;

  public RSIUnder30(RSIIndicator rsiIndicator) {
    this.rsiIndicator = rsiIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    return rsiIndicator.getValue(index).isLessThan(30);
  }
}
