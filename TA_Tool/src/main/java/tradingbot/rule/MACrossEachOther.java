package tradingbot.rule;

import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class MACrossEachOther extends AbstractRule {

  SMAIndicator fastIndicator;
  SMAIndicator lowIndicator;

  public MACrossEachOther(SMAIndicator fastIndicator, SMAIndicator lowIndicator) {
    this.fastIndicator = fastIndicator;
    this.lowIndicator = lowIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    return fastIndicator.getValue(index).isGreaterThanOrEqual(fastIndicator.getValue(index-1))
    && fastIndicator.getValue(index-2).isLessThan(lowIndicator.getValue(index-2))
    && fastIndicator.getValue(index).isGreaterThanOrEqual(lowIndicator.getValue(index));
  }
}
