package tradingbot.rule;

import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

/**
 * Created by truongnhukhang on 6/3/18.
 */
public class MACDCross extends AbstractRule {

  MACDIndicator macdIndicator;
  SMAIndicator signalIndicator;

  public MACDCross(MACDIndicator macdIndicator) {
    this.macdIndicator = macdIndicator;
    this.signalIndicator = new SMAIndicator(macdIndicator,5);
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    return macdIndicator.getValue(index).isGreaterThanOrEqual(macdIndicator.getValue(index-1))
    && macdIndicator.getValue(index-2).isLessThanOrEqual(signalIndicator.getValue(index-2))
        && macdIndicator.getValue(index).isGreaterThanOrEqual(signalIndicator.getValue(index));
  }
}
