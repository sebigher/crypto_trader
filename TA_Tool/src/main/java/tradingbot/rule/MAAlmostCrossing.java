package tradingbot.rule;

import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class MAAlmostCrossing extends AbstractRule {

  SMAIndicator fastIndicator;
  SMAIndicator lowIndicator;

  public MAAlmostCrossing(SMAIndicator fastIndicator, SMAIndicator lowIndicator) {
    this.fastIndicator = fastIndicator;
    this.lowIndicator = lowIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal fastValue = fastIndicator.getValue(index);
    Decimal lowValue = lowIndicator.getValue(index);
    return fastIndicator.getValue(index).isGreaterThanOrEqual(fastIndicator.getValue(index-1))
    && fastValue.isLessThan(lowValue)
        && fastValue.plus(lowValue.multipliedBy(0.005)).isGreaterThanOrEqual(lowValue);
  }
}
