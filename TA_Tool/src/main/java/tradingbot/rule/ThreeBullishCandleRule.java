package tradingbot.rule;

import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.trading.rules.AbstractRule;

public class ThreeBullishCandleRule extends AbstractRule {

  Indicator indicator;

  public ThreeBullishCandleRule(Indicator indicator) {
    this.indicator = indicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    TimeSeries timeSeries = indicator.getTimeSeries();
    return timeSeries.getBar(index).isBullish()
        && timeSeries.getBar(index-1).isBullish()
        && timeSeries.getBar(index-2).isBullish();
  }
}
