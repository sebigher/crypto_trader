package tradingbot.indicator;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.indicators.CachedIndicator;
import org.ta4j.core.indicators.EMAIndicator;

public class MACDHistogramIndicator extends CachedIndicator<Decimal> {
    private static final long serialVersionUID = -6899062131135971403L;


    private final EMAIndicator shortTermEma;
    private final EMAIndicator longTermEma;
    private final EMAIndicator signalLine;

    public MACDHistogramIndicator(Indicator<Decimal> indicator, Indicator<Decimal> macdIndicator) {
        super(indicator);
        shortTermEma = new EMAIndicator(indicator,12);
        longTermEma = new EMAIndicator(indicator,26);
        signalLine = new EMAIndicator(macdIndicator,5);
    }



    @Override
    protected Decimal calculate(int index) {
        return (shortTermEma.getValue(index).minus(longTermEma.getValue(index))).minus(signalLine.getValue(index));
    }
}
