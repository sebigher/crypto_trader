package tradingbot.repository;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import tradingbot.model.SignalPrediction;
import tradingbot.model.SignalPredictionLearning;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Created by truongnhukhang on 3/28/18.
 */
public class FireStoreRepository {
    Firestore db = null;
    CollectionReference bumpCollection = null;
    CollectionReference analyzerCollection = null;
    public FireStoreRepository() {
        InputStream serviceAccount = null;
        try {
            serviceAccount = FireStoreRepository.class.getResourceAsStream("/kubi-bot-key.json");
            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(credentials)
                    .build();
            FirebaseApp firebaseApp = null;
            if(FirebaseApp.getApps().size()==0) {
              firebaseApp = FirebaseApp.initializeApp(options,"kubi_bot");
            } else {
              firebaseApp = FirebaseApp.getApps().get(0);
            }
            db = FirestoreClient.getFirestore(firebaseApp);
            bumpCollection = db.collection("Luna_data");
            analyzerCollection = db.collection("learning_data");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }

    public void savePrediction(SignalPrediction signalPrediction) throws ExecutionException, InterruptedException {
        bumpCollection.document(signalPrediction.getSymbol()).set(signalPrediction).get().getUpdateTime();
    }

    public void saveLearningPrediction(SignalPredictionLearning signalPredictionLearning) throws ExecutionException, InterruptedException {
        analyzerCollection.document(signalPredictionLearning.getSignalPrediction().getSymbol()+"_"+signalPredictionLearning.getSignalPrediction().getLastUpdateTime()+"_"+signalPredictionLearning.getAlgorithm()).set(signalPredictionLearning).get().getUpdateTime();
    }

    public List<SignalPrediction> getSignalPredictions() throws ExecutionException, InterruptedException {
        List<QueryDocumentSnapshot> documents = bumpCollection.get().get().getDocuments();
        return documents.stream().map(queryDocumentSnapshot -> queryDocumentSnapshot.toObject(SignalPrediction.class)).collect(Collectors.toList());
    }

    public SignalPrediction getSignalPredictionBySymbol(String symbol) throws ExecutionException, InterruptedException {
        return bumpCollection.document(symbol).get().get().toObject(SignalPrediction.class);
    }

    public List<SignalPredictionLearning> getSignalPredictionLearningEqualZero() throws ExecutionException, InterruptedException {
        return analyzerCollection.whereEqualTo("realPriceAfter5min",0).get().get().toObjects(SignalPredictionLearning.class);
    }

}
