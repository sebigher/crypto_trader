package tradingbot.repository;

import tradingbot.model.GroupCandle;

import java.util.List;

/**
 * Created by truongnhukhang on 6/26/18.
 */
public interface GroupCandleSearchRepository {
  List<GroupCandle> findGroupCandlesBySimilarSignal(String operator,
                                                    List<String> s30Signals, List<String> s60Signals, List<String> s120Signals, List<String> s240Signals, List<String> s1440Signals,
                                                    Integer score_30, Integer score_60, Integer score_120, Integer score_240, Integer score_1440, int limit, int skip,
                                                    Long opentime);
  Iterable<Long> findGroupCandleIdsWithFuturePriceEqualZero();
}
