package tradingbot.worker;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.SymbolInfo;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.TimeSeries;
import tradingbot.model.SignalPrediction;
import tradingbot.reader.BinanceReader;
import tradingbot.reader.BinanceVolumeReader;
import tradingbot.reader.ExchangeReader;
import tradingbot.repository.FireStoreRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Component
public class AnalyzerWorker {
  public FireStoreRepository fireStoreRepository = new FireStoreRepository();
  List<SignalPrediction> signalPredictions = new ArrayList<>();
  public Map<String, ExchangeReader> exchangeReaderMap = new ConcurrentHashMap<>();
  List<Callable> callables = new ArrayList<>();
  ExecutorService executor = Executors.newFixedThreadPool(5);
  List<String> symbols;

  public void prepareAnalyze() {
    System.out.println("Prepare callables");
    BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
    BinanceApiRestClient client = factory.newRestClient();
    fireStoreRepository = new FireStoreRepository();
    symbols = client.getExchangeInfo().getSymbols().stream().map(SymbolInfo::getSymbol).collect(Collectors.toList());
    for (int i = 0; i < symbols.size(); i++) {
      String symbol = symbols.get(i);
      if (symbol.endsWith("BTC")) {
        SignalPrediction signalPrediction = new SignalPrediction();
        signalPrediction.setSymbol(symbol);

        TimeSeries timeSeries_30 = new BaseTimeSeries(symbol);
        timeSeries_30.setMaximumBarCount(100);
        ExchangeReader exchangeReader_30 = new BinanceReader(timeSeries_30, signalPrediction);

        exchangeReaderMap.put(symbol + "_30", exchangeReader_30);

        TimeSeries timeSeries_60 = new BaseTimeSeries(symbol);
        timeSeries_60.setMaximumBarCount(100);
        ExchangeReader exchangeReader_60 = new BinanceReader(timeSeries_60, signalPrediction);
        exchangeReaderMap.put(symbol + "_60", exchangeReader_60);

        TimeSeries timeSeries_120 = new BaseTimeSeries(symbol);
        timeSeries_120.setMaximumBarCount(100);
        ExchangeReader exchangeReader_120 = new BinanceReader(timeSeries_120, signalPrediction);
        exchangeReaderMap.put(symbol + "_120", exchangeReader_120);

        TimeSeries timeSeries_240 = new BaseTimeSeries(symbol);
        timeSeries_240.setMaximumBarCount(100);
        ExchangeReader exchangeReader_240 = new BinanceReader(timeSeries_240, signalPrediction);
        exchangeReaderMap.put(symbol + "_240", exchangeReader_240);


        TimeSeries timeSeries_1440 = new BaseTimeSeries(symbol);
        timeSeries_1440.setMaximumBarCount(100);
        ExchangeReader exchangeReader_1440 = new BinanceReader(timeSeries_1440, signalPrediction);
        exchangeReaderMap.put(symbol + "_1440", exchangeReader_1440);

        signalPredictions.add(signalPrediction);
        Callable callable = () -> {
          try {
            SignalPrediction signalPredictionCurrent = fireStoreRepository.getSignalPredictionBySymbol(symbol);
            exchangeReader_30.readBarFromExchange(symbol, 30);
            exchangeReader_60.readBarFromExchange(symbol, 60);
            exchangeReader_120.readBarFromExchange(symbol, 120);
            exchangeReader_240.readBarFromExchange(symbol, 240);
            exchangeReader_1440.readBarFromExchange(symbol, 1440);
            createBuySellData(signalPrediction);
            if(signalPredictionCurrent!=null) {
              signalPrediction.setPriceAfter5min(signalPredictionCurrent.getPriceAfter5min());
              signalPrediction.setPriceAfter10min(signalPredictionCurrent.getPriceAfter10min());
              signalPrediction.setPriceAfter15min(signalPredictionCurrent.getPriceAfter15min());
              signalPrediction.setPriceAfter30min(signalPredictionCurrent.getPriceAfter30min());
              signalPrediction.setPriceAfter60min(signalPredictionCurrent.getPriceAfter60min());
              signalPrediction.setPriceAfter120min(signalPredictionCurrent.getPriceAfter120min());
              signalPrediction.setPriceAfter240min(signalPredictionCurrent.getPriceAfter240min());
              signalPrediction.setPriceAfter480min(signalPredictionCurrent.getPriceAfter480min());
              signalPrediction.setPriceAfter1440min(signalPredictionCurrent.getPriceAfter1440min());
              signalPrediction.setPriceAfter2480min(signalPredictionCurrent.getPriceAfter2480min());
            }
            fireStoreRepository.savePrediction(signalPrediction);
          } catch (ExecutionException e) {
            e.printStackTrace();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          return "OK";
        };
        callables.add(callable);
        // sleep to avoid limit rate binance
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    System.out.println("Finish prepare ");
  }

  @Scheduled(cron = "0 0/10 * * * *")
  public void updateTAJob()  {
    Date startTime = new Date();
    System.out.println("Start Update TA Job : " + startTime);
    for (int i = 1; i <= callables.size(); i++) {
      try {
        callables.get(i - 1).call();
      } catch (Exception e) {
        e.printStackTrace();
      }
      if ((i * 6) % 1200 == 0) {
        try {
          Thread.sleep(60000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    Date endTime = new Date();
    System.out.println("End UpdateTAJob : " + endTime + " | it took : " + (endTime.getTime() - startTime.getTime()) / 1000 / 60 + " minutes");
  }

  private void createBuySellData(SignalPrediction signalPrediction) {
    BinanceVolumeReader binanceVolumeReader = new BinanceVolumeReader();
    binanceVolumeReader.buildRecentTradeAndBuySellVolume(signalPrediction);
    signalPrediction.setLastUpdateTime(new Date().getTime());
  }
}