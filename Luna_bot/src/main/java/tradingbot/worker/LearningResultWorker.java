package tradingbot.worker;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import tradingbot.model.GroupCandle;
import tradingbot.model.SignalPredictionLearning;
import tradingbot.repository.FireStoreRepository;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

public class LearningResultWorker {
  FireStoreRepository fireStoreRepository = new FireStoreRepository();
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient client = factory.newRestClient();
  List<Callable<SignalPredictionLearning>> callables = new ArrayList<>();

  public void generateLearningResult() {
    try {
      System.out.println("Start : " + new Date());
      ExecutorService executor = Executors.newWorkStealingPool();
      List<SignalPredictionLearning> signalPredictionLearnings = fireStoreRepository.getSignalPredictionLearningEqualZero();
      System.out.println("Total Item need to request : " + signalPredictionLearnings.size());
      signalPredictionLearnings.forEach(this::buildCallables);
      runCallables(executor, callables);
      System.out.println("Start Saving : " + new Date());
      signalPredictionLearnings.forEach(signalPredictionLearning -> {
        try {
          fireStoreRepository.saveLearningPrediction(signalPredictionLearning);
        } catch (ExecutionException e) {
          e.printStackTrace();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      });
      System.out.println("End : " + new Date());
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void buildCallables(SignalPredictionLearning signalPredictionLearning) {
    Callable<SignalPredictionLearning> signalPredictionLearningCallable = new Callable<SignalPredictionLearning>() {
      @Override
      public SignalPredictionLearning call() throws Exception {
        return buildRealFuturePrice(signalPredictionLearning);
      }
    };
    callables.add(signalPredictionLearningCallable);
  }

  private void runCallables(ExecutorService executor, List<Callable<SignalPredictionLearning>> callables) {
    int flag = 0;
    for (int i = 0; i < callables.size(); i++) {
      if (i != 0 && i % 1200 == 0) {
        try {
          System.out.println("Start call : " + new Date());
          List<Future<SignalPredictionLearning>> futures = executor.invokeAll(callables.subList(flag, i));
          futures.parallelStream().forEach(future -> {
            try {
              future.get();
            } catch (InterruptedException e) {
              e.printStackTrace();
            } catch (ExecutionException e) {
              e.printStackTrace();
            }
          });
          System.out.println("Sleep Before Call API to avoid rate limit, " + ((callables.size() - i)) + " request need to call : " + new Date());
          Thread.sleep(60000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        flag = i;
      }
    }
    if (flag < callables.size() - 1) {
      List<Future<SignalPredictionLearning>> futures = null;
      System.out.println("Get last requests (" + ((callables.size() - flag)) + ")" + new Date());
      try {
        System.out.println("Start call : " + new Date());
        futures = executor.invokeAll(callables.subList(flag, callables.size() - 1));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      futures.parallelStream().forEach(future -> {
        try {
          future.get();
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
      });
    }
  }

  private SignalPredictionLearning buildRealFuturePrice(SignalPredictionLearning signalPredictionLearning) {
    Date candleOpenTime = new Date(signalPredictionLearning.getSignalPrediction().getLastUpdateTime());
    List<Candlestick> candlesticks = client.getCandlestickBars(signalPredictionLearning.getSignalPrediction().getSymbol(), CandlestickInterval.FIVE_MINUTES, null, signalPredictionLearning.getSignalPrediction().getLastUpdateTime(), candleOpenTime.toInstant().plus(2, ChronoUnit.DAYS).toEpochMilli());
    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(5, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(5, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter5min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(10, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(10, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter10min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(15, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(15, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter15min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(30, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(30, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter30min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(60, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(60, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter60min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(120, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(120, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter120min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(240, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(240, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter240min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(480, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(480, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter480min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(1440, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(1440, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter1440min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(2480, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(2480, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> signalPredictionLearning.setRealPriceAfter2480min(Float.parseFloat(candlestick.getClose())));

    return signalPredictionLearning;
  }
}
