package tradingbot.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class BuySellLine {
    String symbol;
    int candlestickInterval;
    long startTime;
    long endTime;
    String startDate;
    String endDate;
    double numberBTC_In;
    double numberBTC_out;
    long tradeId;
    Map<String,Double> buyVolume = new HashMap<>();
    Map<String,Double> sellVolume = new HashMap<>();
    public BuySellLine(int candlestickInterval, long startTime, long endTime) {
        this.candlestickInterval = candlestickInterval;
        this.startTime = startTime;
        this.endTime = endTime;
        this.startDate = new Date(startTime).toString();
        this.endDate = new Date(endTime).toString();
    }

    public int getCandlestickInterval() {
        return candlestickInterval;
    }

    public void setCandlestickInterval(int candlestickInterval) {
        this.candlestickInterval = candlestickInterval;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double getNumberBTC_In() {
        return numberBTC_In;
    }

    public void setNumberBTC_In(double numberBTC_In) {
        this.numberBTC_In = numberBTC_In;
    }

    public double getNumberBTC_out() {
        return numberBTC_out;
    }

    public void setNumberBTC_out(double numberBTC_out) {
        this.numberBTC_out = numberBTC_out;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Map<String, Double> getBuyVolume() {
        return buyVolume;
    }

    public void setBuyVolume(Map<String, Double> buyVolume) {
        this.buyVolume = buyVolume;
    }

    public Map<String, Double> getSellVolume() {
        return sellVolume;
    }

    public void setSellVolume(Map<String, Double> sellVolume) {
        this.sellVolume = sellVolume;
    }

    public long getTradeId() {
        return tradeId;
    }

    public void setTradeId(long tradeId) {
        this.tradeId = tradeId;
    }
}
