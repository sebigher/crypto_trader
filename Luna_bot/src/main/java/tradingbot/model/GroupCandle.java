package tradingbot.model;

import org.neo4j.ogm.annotation.*;
import tradingbot.SignalType;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by truongnhukhang on 6/18/18.
 */
@NodeEntity
public class GroupCandle {
  @Id
  @GeneratedValue
  Long id;
  String symbol;
  long openTime;
  @Relationship(type = "HAVE_CANDLE_30")
  Candle interval30Min;
  @Relationship(type = "HAVE_CANDLE_60")
  Candle interval60Min;
  @Relationship(type = "HAVE_CANDLE_120")
  Candle interval120Min;
  @Relationship(type = "HAVE_CANDLE_240")
  Candle interval240Min;
  @Relationship(type = "HAVE_CANDLE_1440")
  Candle interval1440Min;
  @Relationship(type = "HAVE_SIGNALS_30")
  Set<Signal> signals30min;
  @Relationship(type = "HAVE_SIGNALS_60")
  Set<Signal> signals60min;
  @Relationship(type = "HAVE_SIGNALS_120")
  Set<Signal> signals120min;
  @Relationship(type = "HAVE_SIGNALS_240")
  Set<Signal> signals240min;
  @Relationship(type = "HAVE_SIGNALS_1440")
  Set<Signal> signals1440min;
  float priceAfter5min;
  float priceAfter10min;
  float priceAfter15min;
  float priceAfter30min;
  float priceAfter60min;
  float priceAfter120min;
  float priceAfter240min;
  float priceAfter480min;
  float priceAfter1440min;
  float priceAfter2480min;
  List<Float> btcMA5_4h;
  List<Float> btcMA5_1d;
  @Transient
  Integer totalScore;
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public long getOpenTime() {
    return openTime;
  }

  public void setOpenTime(long openTime) {
    this.openTime = openTime;
  }

  public Candle getInterval30Min() {
    return interval30Min;
  }

  public void setInterval30Min(Candle interval30Min) {
    this.interval30Min = interval30Min;
  }

  public Candle getInterval60Min() {
    return interval60Min;
  }

  public void setInterval60Min(Candle interval60Min) {
    this.interval60Min = interval60Min;
  }

  public Candle getInterval120Min() {
    return interval120Min;
  }

  public void setInterval120Min(Candle interval120Min) {
    this.interval120Min = interval120Min;
  }

  public Candle getInterval240Min() {
    return interval240Min;
  }

  public void setInterval240Min(Candle interval240Min) {
    this.interval240Min = interval240Min;
  }

  public Candle getInterval1440Min() {
    return interval1440Min;
  }

  public void setInterval1440Min(Candle interval1440Min) {
    this.interval1440Min = interval1440Min;
  }

  public Set<Signal> getSignals30min() {
    return signals30min;
  }

  public void setSignals30min(Set<Signal> signals30min) {
    this.signals30min = signals30min;
  }

  public Set<Signal> getSignals60min() {
    return signals60min;
  }

  public void setSignals60min(Set<Signal> signals60min) {
    this.signals60min = signals60min;
  }

  public Set<Signal> getSignals120min() {
    return signals120min;
  }

  public void setSignals120min(Set<Signal> signals120min) {
    this.signals120min = signals120min;
  }

  public Set<Signal> getSignals240min() {
    return signals240min;
  }

  public void setSignals240min(Set<Signal> signals240min) {
    this.signals240min = signals240min;
  }

  public Set<Signal> getSignals1440min() {
    return signals1440min;
  }

  public void setSignals1440min(Set<Signal> signals1440min) {
    this.signals1440min = signals1440min;
  }

  public float getPriceAfter5min() {
    return priceAfter5min;
  }

  public void setPriceAfter5min(float priceAfter5min) {
    this.priceAfter5min = priceAfter5min;
  }

  public float getPriceAfter10min() {
    return priceAfter10min;
  }

  public void setPriceAfter10min(float priceAfter10min) {
    this.priceAfter10min = priceAfter10min;
  }

  public float getPriceAfter15min() {
    return priceAfter15min;
  }

  public void setPriceAfter15min(float priceAfter15min) {
    this.priceAfter15min = priceAfter15min;
  }

  public float getPriceAfter30min() {
    return priceAfter30min;
  }

  public void setPriceAfter30min(float priceAfter30min) {
    this.priceAfter30min = priceAfter30min;
  }

  public float getPriceAfter60min() {
    return priceAfter60min;
  }

  public void setPriceAfter60min(float priceAfter60min) {
    this.priceAfter60min = priceAfter60min;
  }

  public float getPriceAfter120min() {
    return priceAfter120min;
  }

  public void setPriceAfter120min(float priceAfter120min) {
    this.priceAfter120min = priceAfter120min;
  }

  public float getPriceAfter240min() {
    return priceAfter240min;
  }

  public void setPriceAfter240min(float priceAfter240min) {
    this.priceAfter240min = priceAfter240min;
  }

  public float getPriceAfter480min() {
    return priceAfter480min;
  }

  public void setPriceAfter480min(float priceAfter480min) {
    this.priceAfter480min = priceAfter480min;
  }

  public float getPriceAfter1440min() {
    return priceAfter1440min;
  }

  public void setPriceAfter1440min(float priceAfter1440min) {
    this.priceAfter1440min = priceAfter1440min;
  }

  public float getPriceAfter2480min() {
    return priceAfter2480min;
  }

  public void setPriceAfter2480min(float priceAfter2480min) {
    this.priceAfter2480min = priceAfter2480min;
  }

  public List<Float> getBtcMA5_4h() {
    return btcMA5_4h;
  }

  public void setBtcMA5_4h(List<Float> btcMA5_4h) {
    this.btcMA5_4h = btcMA5_4h;
  }

  public List<Float> getBtcMA5_1d() {
    return btcMA5_1d;
  }

  public void setBtcMA5_1d(List<Float> btcMA5_1d) {
    this.btcMA5_1d = btcMA5_1d;
  }

  public Integer getTotalScore() {
    int score_30 = 0;
    int score_60 = 0;
    int score_120 = 0;
    int score_240 = 0;
    int score_1440 = 0;

    if(signals30min!=null && signals30min.size()>0) {
      List<SignalType> signalTypes_30 = signals30min.stream()
          .map(convertSignalToSignalType()).collect(Collectors.toList());
      score_30 = signalTypes_30.stream().map(SignalType::getScore).reduce(Integer::sum).get();
    }

    if(signals60min!=null && signals60min.size()>0) {
      List<SignalType> signalTypes_60 = signals60min.stream()
          .map(convertSignalToSignalType()).collect(Collectors.toList());
      score_60 = signalTypes_60.stream().map(SignalType::getScore).reduce(Integer::sum).get();
    }
    if(signals120min!=null && signals120min.size()>0) {
      List<SignalType> signalTypes_120 = signals120min.stream()
          .map(convertSignalToSignalType()).collect(Collectors.toList());
      score_120 = signalTypes_120.stream().map(SignalType::getScore).reduce(Integer::sum).get();

    }
    if(signals240min!=null && signals240min.size()>0) {
      List<SignalType> signalTypes_240 = signals240min.stream()
          .map(convertSignalToSignalType()).collect(Collectors.toList());
      score_240 = signalTypes_240.stream().map(SignalType::getScore).reduce(Integer::sum).get();

    }
    if(signals1440min!=null && signals1440min.size()>0) {
      List<SignalType> signalTypes_1440 = signals1440min.stream()
          .map(convertSignalToSignalType()).collect(Collectors.toList());
      score_1440 = signalTypes_1440.stream().map(SignalType::getScore).reduce(Integer::sum).get();

    }
    totalScore = score_30 + score_60*2 + score_120*3 + score_240*4 + score_1440*5;
    return totalScore;
  }

  public float getFuturePriceChangePercentBaseOnTotalScore(int period, Integer totalScore) {
    float similarPercent = (float) (totalScore*1.0/this.getTotalScore()*1.0);
    float currentPrice = interval30Min.getClose();
    switch (period) {
      case 5 :
        return (((priceAfter5min-currentPrice)/currentPrice)*100)*similarPercent;
      case 10 :
        return (((priceAfter10min-currentPrice)/currentPrice)*100)*similarPercent;
      case 15 :
        return (((priceAfter15min-currentPrice)/currentPrice)*100)*similarPercent;
      case 30 :
        return (((priceAfter30min-currentPrice)/currentPrice)*100)*similarPercent;
      case 60 :
        return (((priceAfter60min-currentPrice)/currentPrice)*100)*similarPercent;
      case 120 :
        return (((priceAfter120min-currentPrice)/currentPrice)*100)*similarPercent;
      case 240 :
        return (((priceAfter240min-currentPrice)/currentPrice)*100)*similarPercent;
      case 480 :
        return (((priceAfter480min-currentPrice)/currentPrice)*100)*similarPercent;
      case 1440 :
        return (((priceAfter1440min-currentPrice)/currentPrice)*100)*similarPercent;
      case 2480 :
        return (((priceAfter2480min-currentPrice)/currentPrice)*100)*similarPercent;
    }
    return 0;
  }

  private Function<Signal, SignalType> convertSignalToSignalType() {
    return s -> {
      return Stream.of(SignalType.values()).filter(signalType -> signalType.toString().equals(s.getType())).findFirst().get();
    };
  }

  public void setTotalScore(Integer totalScore) {
    this.totalScore = totalScore;
  }
}
