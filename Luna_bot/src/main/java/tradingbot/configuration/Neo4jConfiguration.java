package tradingbot.configuration;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by truongnhukhang on 6/13/18.
 */
@Configuration
@EnableNeo4jRepositories(basePackages = {"tradingbot.model","tradingbot.repository"})
@EnableTransactionManagement
public class Neo4jConfiguration {
  @Bean
  public SessionFactory sessionFactory() {
    // with domain entity base package(s)
    return new SessionFactory(configuration(), "tradingbot.model");
  }

  @Bean
  public org.neo4j.ogm.config.Configuration configuration() {
    org.neo4j.ogm.config.Configuration configuration = new org.neo4j.ogm.config.Configuration.Builder()
        .uri("bolt://35.185.234.70:7687")
        .credentials("neo4j", "khang123")
        //.encryptionLevel("REQUIRED")
//        .trustStrategy("TRUST_ON_FIRST_USE").trustCertFile("/tmp/cert")
        .build();
    return configuration;
  }

  @Bean
  public Neo4jTransactionManager transactionManager() {
    return new Neo4jTransactionManager(sessionFactory());
  }
}
