package tradingbot.reader;

import java.util.Date;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public interface ExchangeReader {
    void readBarFromExchange(String code, int candleTime);

}
