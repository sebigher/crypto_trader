package tradingbot.reader;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.AggTrade;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by truongnhukhang on 3/22/18.
 */
public class BinanceBackTestReader implements ExchangeReader {
    public static final String History_Location = "C:\\historyCandle_";
    public static final String History_Location_MAC = "/Users/truongnhukhang/work/data_history/historyCandle_";
    BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
    BinanceApiRestClient client = factory.newRestClient();
    TimeSeries timeSeries;
    Long startTime;
    Long endTime;
    List<Candlestick> historyCandle = new ArrayList<>();
    public BinanceBackTestReader(TimeSeries timeSeries, Long startTime, Long endTime) {
        this.timeSeries = timeSeries;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public void readBarFromExchange(String code, int candleTime) {
        System.out.println("start : " + startTime);
        System.out.println("end : " + endTime);
        for (long i = startTime; i < endTime; i=i+30000000) {
            long endTimeOfCandle = endTime - i > 30000000 ? i+30000000 : endTime;
            System.out.println("Start get data at : " + i + "-" + endTimeOfCandle);
            List<Candlestick> candlesticks =  client.getCandlestickBars(code,
                    CandlestickInterval.ONE_MINUTE,500,i,endTimeOfCandle
                    );
            historyCandle.addAll(candlesticks);
        }
        historyCandle.stream().map(candlestick -> {
            Bar bar = new BaseBar(Duration.ofMinutes(candleTime),
                    Instant.ofEpochMilli(candlestick.getOpenTime()).atZone(ZoneOffset.UTC),
                    Decimal.valueOf(candlestick.getOpen()),
                    Decimal.valueOf(candlestick.getHigh()),
                    Decimal.valueOf(candlestick.getLow()),
                    Decimal.valueOf(candlestick.getClose()),
                    Decimal.valueOf(candlestick.getVolume()));
            return bar;

        }).forEach(timeSeries::addBar);
        ObjectMapper mapper = new ObjectMapper();
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = new Date(startTime);
            Date endDate = new Date(endTime);
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(History_Location + code +"_" + df.format(startDate) + "_" + df.format(endDate)+".json")
                    ,historyCandle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void readTradeFromExchange(String code,Date from, Date to) {
//        System.out.println("start collect " + code + " From : " + from + " to : " + to);
//        long fromDate = from.getTime();
//        long toDate = to.getTime();
//        long lastTradeTime = 0;
//        List<AggTrade> aggTrades = client.getAggTrades(code,null,null,fromDate,fromDate+3600000);
//        if(aggTrades.size()>0) {
//            lastTradeTime = aggTrades.get(aggTrades.size() - 1).getTradeTime();
//        } else {
//            lastTradeTime = fromDate;
//        }
//        while(lastTradeTime < toDate) {
//            System.out.println("get Data : " + new Date(lastTradeTime));
//            aggTrades.addAll(client.getAggTrades(code,null,null,lastTradeTime,lastTradeTime+3600000));
//            lastTradeTime = lastTradeTime+3600000;
//        }
//        System.out.println("Start write Json");
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(History_Location + code +"_TRADE_HISTORY_" + df.format(fromDate) + "_" + df.format(toDate)+".json")
//                    ,aggTrades);
//        } catch (IOException e) {
//            System.out.println("Error Code : " + code);
//            e.printStackTrace();
//        }
//        aggTrades = null;
//        System.out.println("Finish collect " + code);
//    }
//
//    @Override
//    public void readTradeFromJsonFile(String location) {
//
//    }

    public void readBarFromJsonfile(String location) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            historyCandle = mapper.readValue(new File(location),new TypeReference<List<Candlestick>>(){});
            historyCandle.stream().map(candlestick -> {
                Bar bar = new BaseBar(Duration.ofMinutes(1),
                    Instant.ofEpochMilli(candlestick.getOpenTime()).atZone(ZoneOffset.UTC),
                    Decimal.valueOf(candlestick.getOpen()),
                    Decimal.valueOf(candlestick.getHigh()),
                    Decimal.valueOf(candlestick.getLow()),
                    Decimal.valueOf(candlestick.getClose()),
                    Decimal.valueOf(candlestick.getVolume()));
                return bar;

            }).forEach(timeSeries::addBar);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
