package tradingbot.reader;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import org.ta4j.core.Decimal;
import tradingbot.model.SignalPrediction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by truongnhukhang on 6/3/18.
 */
public class BinanceVolumeReader {
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");

  BinanceApiRestClient clientRest = factory.newRestClient();

  public double buildBuySellVolume(List<Map<String, String>> trades, int period, long latestTime) {
    double volume = 0;
    long startTime = 0;
    if (period != -1) {
      startTime = latestTime - (period * 60000);
    }
    long finalStartTime = startTime;
    trades = trades.stream().filter(trade -> Long.valueOf(trade.get("Time")) > finalStartTime).collect(Collectors.toList());
    for (int i = 0; i < trades.size(); i++) {
      Map<String, String> buyEvent = trades.get(i);
      volume = volume + Decimal.valueOf(buyEvent.get("Quantity")).multipliedBy(Decimal.valueOf(buyEvent.get("Price"))).doubleValue();
    }
    return volume;
  }

  public double buildBuySellVolumeSymbol(List<Map<String, String>> trades, int period, long latestTime) {
    double volume = 0;
    long startTime = 0;
    if (period != -1) {
      startTime = latestTime - (period * 60000);
    }
    long finalStartTime = startTime;
    trades = trades.stream().filter(trade -> Long.valueOf(trade.get("Time")) > finalStartTime).collect(Collectors.toList());
    for (int i = 0; i < trades.size(); i++) {
      Map<String, String> buyEvent = trades.get(i);
      volume = volume + Decimal.valueOf(buyEvent.get("Quantity")).doubleValue();
    }
    return volume;
  }

  public void buildRecentTradeAndBuySellVolume(SignalPrediction signalPrediction) {
    long latestTime = System.currentTimeMillis();
    long startTime = latestTime - 3600000;
    List<Map<String, String>> recentTrades = clientRest.getAggTrades(signalPrediction.getSymbol(), null, null, startTime, latestTime)
        .stream().map(aggTrade -> {
          Map<String, String> tradeEvent = new HashMap<>();
          tradeEvent.put("Type", aggTrade.isBuyerMaker() ? "S" : "B");
          tradeEvent.put("Quantity", aggTrade.getQuantity());
          tradeEvent.put("Time", String.valueOf(aggTrade.getTradeTime()));
          tradeEvent.put("Price", aggTrade.getPrice());
          return tradeEvent;
        }).collect(Collectors.toList());
//        pumpSignal.setRecentTrades(recentTrades);
    List<Map<String, String>> buyTrades = recentTrades.stream().filter(tradeEvent -> tradeEvent.get("Type").equals("B")).collect(Collectors.toList());
    List<Map<String, String>> sellTrades = recentTrades.stream().filter(tradeEvent -> tradeEvent.get("Type").equals("S")).collect(Collectors.toList());
    double buy_30min_Volume = buildBuySellVolume(buyTrades, 30, latestTime);
    double buy_30min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 30, latestTime);
    double sell_30min_Volume = buildBuySellVolume(sellTrades, 30, latestTime);
    double sell_30min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 30, latestTime);
    double buy_60min_Volume = buildBuySellVolume(buyTrades, 60, latestTime);
    double buy_60min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 60, latestTime);
    double sell_60min_Volume = buildBuySellVolume(sellTrades, 60, latestTime);
    double sell_60min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 60, latestTime);
    double buy_15min_Volume = buildBuySellVolume(buyTrades, 15, latestTime);
    double buy_15min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 15, latestTime);
    double sell_15min_Volume = buildBuySellVolume(sellTrades, 15, latestTime);
    double sell_15min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 15, latestTime);
    double buy_5min_Volume = buildBuySellVolume(buyTrades, 5, latestTime);
    double buy_5min_Volume_Symbol = buildBuySellVolumeSymbol(buyTrades, 5, latestTime);
    double sell_5min_Volume = buildBuySellVolume(sellTrades, 5, latestTime);
    double sell_5min_Volume_Symbol = buildBuySellVolumeSymbol(sellTrades, 5, latestTime);
    Map<String, Double> buyVolume = new TreeMap<>();
    Map<String, Double> sellVolume = new TreeMap<>();
    buyVolume.put("5", buy_5min_Volume);
    buyVolume.put("15", buy_15min_Volume);
    buyVolume.put("30", buy_30min_Volume);
    buyVolume.put("60", buy_60min_Volume);
    sellVolume.put("5", sell_5min_Volume);
    sellVolume.put("15", sell_15min_Volume);
    sellVolume.put("30", sell_30min_Volume);
    sellVolume.put("60", sell_60min_Volume);
    signalPrediction.setRecentBuyVolume(buyVolume);
    signalPrediction.setRecentSellVolume(sellVolume);
    Map<String, Double> buySymbolVolume = new TreeMap<>();
    Map<String, Double> sellSymbolVolume = new TreeMap<>();
    buySymbolVolume.put("5", buy_5min_Volume_Symbol);
    buySymbolVolume.put("15", buy_15min_Volume_Symbol);
    buySymbolVolume.put("30", buy_30min_Volume_Symbol);
    buySymbolVolume.put("60", buy_60min_Volume_Symbol);
    sellSymbolVolume.put("5", sell_5min_Volume_Symbol);
    sellSymbolVolume.put("15", sell_15min_Volume_Symbol);
    sellSymbolVolume.put("30", sell_30min_Volume_Symbol);
    sellSymbolVolume.put("60", sell_60min_Volume_Symbol);
    signalPrediction.setRecentBuySymbolVolume(buySymbolVolume);
    signalPrediction.setRecentSellSymbolVolume(sellSymbolVolume);
  }
}
