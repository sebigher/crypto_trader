package tradingbot.converter;

import tradingbot.dto.GroupCandleDto;
import tradingbot.model.GroupCandle;
import tradingbot.model.Signal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Converter {
  public static GroupCandleDto from(Iterable<GroupCandle> btcGroupCandle,
                                    GroupCandle groupCandle) {
    GroupCandleDto groupCandleDto = new GroupCandleDto();
    groupCandleDto.setSymbol(groupCandle.getSymbol());
    Map<String,List<String>> bullishSignal = new ConcurrentHashMap<>();
    bullishSignal.put("30",groupCandle.getSignals30min()==null ? new ArrayList<>() : groupCandle.getSignals30min().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("60",groupCandle.getSignals60min()==null ? new ArrayList<>() : groupCandle.getSignals60min().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("120",groupCandle.getSignals120min()==null ? new ArrayList<>() : groupCandle.getSignals120min().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("240",groupCandle.getSignals240min()==null ? new ArrayList<>() : groupCandle.getSignals240min().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("1440",groupCandle.getSignals1440min()==null ? new ArrayList<>() : groupCandle.getSignals1440min().stream().map(Signal::getType).collect(Collectors.toList()));
    groupCandleDto.setBullishSignal(bullishSignal);
    groupCandleDto.setCloseTime(groupCandle.getInterval30Min().getCloseTime());
    groupCandleDto.setPrice(groupCandle.getInterval30Min().getClose());
    groupCandleDto.setPriceAfter5min(groupCandle.getPriceAfter5min());
    groupCandleDto.setPriceAfter10min(groupCandle.getPriceAfter10min());
    groupCandleDto.setPriceAfter15min(groupCandle.getPriceAfter15min());
    groupCandleDto.setPriceAfter30min(groupCandle.getPriceAfter30min());
    groupCandleDto.setPriceAfter60min(groupCandle.getPriceAfter60min());
    groupCandleDto.setPriceAfter120min(groupCandle.getPriceAfter120min());
    groupCandleDto.setPriceAfter240min(groupCandle.getPriceAfter240min());
    groupCandleDto.setPriceAfter480min(groupCandle.getPriceAfter480min());
    groupCandleDto.setPriceAfter1440min(groupCandle.getPriceAfter1440min());
    groupCandleDto.setPriceAfter2480min(groupCandle.getPriceAfter2480min());
    btcGroupCandle.forEach(btc -> {
      if(groupCandle.getOpenTime()==btc.getOpenTime()){
        groupCandleDto.setBtcMA5_1d(btc.getBtcMA5_1d());
        groupCandleDto.setBtcMA5_4h(btc.getBtcMA5_4h());
      }
    } );
    return groupCandleDto;
  };
}
