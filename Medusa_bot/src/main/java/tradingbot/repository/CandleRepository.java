package tradingbot.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;
import tradingbot.model.Candle;

import java.util.List;

@Repository
public interface CandleRepository extends Neo4jRepository<Candle,Long> {
  Candle findCandleBySymbolAndCloseTime(String symbol,long closeTime);
  List<Candle> findCandlesBySymbolInAndCloseTimeInAndIntervalEquals(List<String> symbols,List<Long> closeTimes,Integer interval);

}
