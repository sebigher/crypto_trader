package tradingbot.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import org.springframework.stereotype.Repository;
import tradingbot.model.Signal;

import java.util.List;
public interface SignalRepository extends Neo4jRepository<Signal,Long> {
  Signal findSignalByType(String type);
  List<Signal> findSignalsByTypeIn(List<String> types);
}
