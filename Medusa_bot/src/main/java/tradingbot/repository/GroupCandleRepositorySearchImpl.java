package tradingbot.repository;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tradingbot.model.GroupCandle;

import java.util.*;

/**
 * Created by truongnhukhang on 6/26/18.
 */
@Repository
public class GroupCandleRepositorySearchImpl implements GroupCandleSearchRepository {

  @Autowired
  SessionFactory sessionFactory;

  @Override
  public List<GroupCandle> findGroupCandlesBySimilarSignal(String operator, List<String> s30Signals, List<String> s60Signals, List<String> s120Signals, List<String> s240Signals, List<String> s1440Signals, Integer score_30, Integer score_60, Integer score_120, Integer score_240, Integer score_1440, int limit, int skip, Long opentime) {

    Session session = sessionFactory.openSession();
    String query = "";
    if(s30Signals!=null && !s30Signals.isEmpty()) {
      query = query + "Match(g:GroupCandle)-[hs30:HAVE_SIGNALS_30]->(s30:Signal) where g.openTime >= {opentime} and s30.type in {s30Signals} with g,count(hs30) * {score_30} as numberHS30 ";
    } else {
      query = query + "Match(g:GroupCandle)-[hs30:HAVE_SIGNALS_30]->(s30:Signal) where g.openTime >= {opentime} and hs30 is null with g, 0 as numberHS30 ";
    }
    if(s60Signals!=null && !s60Signals.isEmpty()) {
      query = query + "Match(g)-[hs60:HAVE_SIGNALS_60]->(s60:Signal) where g.openTime >= {opentime} and s60.type in {s60Signals} with g,numberHS30,count(hs60) * {score_60} as numberHS60 ";
    } else {
      query = query + "Match(g)-[hs60:HAVE_SIGNALS_60]->(s60:Signal) where g.openTime >= {opentime} and hs60 is null with g,numberHS30,0 as numberHS60 ";
    }
    if(s120Signals!=null && !s120Signals.isEmpty()) {
      query = query + "Match(g)-[hs120:HAVE_SIGNALS_120]->(s120:Signal) where g.openTime >= {opentime} and s120.type in {s120Signals} with g,numberHS30,numberHS60,count(hs120) * {score_120} as numberHS120  ";
    } else {
      query = query + "Match(g)-[hs120:HAVE_SIGNALS_120]->(s120:Signal) where g.openTime >= {opentime} and hs120 is null with g,numberHS30,numberHS60,0 as numberHS120 ";
    }
    if(s240Signals!=null && !s240Signals.isEmpty()) {
      query = query + "Match(g)-[hs240:HAVE_SIGNALS_240]->(s240:Signal) where g.openTime >= {opentime} and s240.type in {s240Signals} with g,numberHS30,numberHS60,numberHS120,count(hs240) * {score_240} as numberHS240  ";
    } else {
      query = query + "Match(g)-[hs240:HAVE_SIGNALS_240]->(s240:Signal) where g.openTime >= {opentime} and hs240 is null with g,numberHS30,numberHS60,numberHS120,0 as numberHS240 ";
    }
    if(s1440Signals!=null && !s1440Signals.isEmpty()) {
      query = query + "Match(g)-[hs1440:HAVE_SIGNALS_1440]->(s1440:Signal) where g.openTime >= {opentime} and s1440.type in {s1440Signals} with g,(numberHS30 + numberHS60 + numberHS120 + numberHS240 + (count(hs1440) * {score_1440})) as totalScore  ";
    } else {
      query = query + "Match(g)-[hs1440:HAVE_SIGNALS_1440]->(s1440:Signal) where g.openTime >= {opentime} and hs1440 is null  with g,(numberHS30 + numberHS60 + numberHS120 + numberHS240) as totalScore ";
    }
    query = query + " where g.symbol<>'BTCUSDT' return g order by totalScore DESC limit {limit}";
    Map<String,Object> parameters = new HashMap<>();
    if(s30Signals!=null && !s30Signals.isEmpty()) {
      parameters.put("s30Signals",s30Signals);
      parameters.put("score_30",score_30);
    }
    if(s60Signals!=null && !s60Signals.isEmpty()) {
      parameters.put("s60Signals",s60Signals);
      parameters.put("score_60",score_60);
    }
    if(s120Signals!=null && !s120Signals.isEmpty()) {
      parameters.put("s120Signals",s120Signals);
      parameters.put("score_120",score_120);
    }
    if(s240Signals!=null && !s240Signals.isEmpty()) {
      parameters.put("s240Signals",s240Signals);
      parameters.put("score_240",score_240);
    }
    if(s1440Signals!=null && !s1440Signals.isEmpty()) {
      parameters.put("s1440Signals",s1440Signals);
      parameters.put("score_1440",score_1440);
    }
    parameters.put("opentime",opentime);
    parameters.put("limit",limit);
    System.out.println(query);
    Iterable<GroupCandle> groupCandles = session.query(GroupCandle.class,query,parameters);
    if(groupCandles.iterator().hasNext()) {
      List<GroupCandle> groupCandleList = new ArrayList<>();
      groupCandles.forEach(groupCandleList::add);
      return groupCandleList;
    }

    return new ArrayList<>();
  }

  public Iterable<Long> findGroupCandleIdsWithFuturePriceEqualZero() {
    Session session = sessionFactory.openSession();
    String query = "MATCH (n:GroupCandle) where n.priceAfter5min = 0 AND n.symbol<>'BTCUSDT' RETURN id(n)";
    Iterable<Long> groupCandleIds = session.query(Long.class,query,new HashMap<>());
    return groupCandleIds;

  }
}
