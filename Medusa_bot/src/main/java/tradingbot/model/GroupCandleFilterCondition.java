package tradingbot.model;

import java.util.List;
import java.util.Map;

public class GroupCandleFilterCondition {
  private Map<Integer,List<String>> signalListByInterval;
  private Map<Integer,Integer> scoreByInterval;
  private float currentPrice;
  private Long opentime;
  public Integer page = 1 ;
  public Integer size = 10;

  public Long getOpentime() {
    return opentime;
  }

  public void setOpentime(Long opentime) {
    this.opentime = opentime;
  }

  public Map<Integer, List<String>> getSignalListByInterval() {
    return signalListByInterval;
  }

  public void setSignalListByInterval(Map<Integer, List<String>> signalListByInterval) {
    this.signalListByInterval = signalListByInterval;
  }

  public Map<Integer, Integer> getScoreByInterval() {
    return scoreByInterval;
  }

  public void setScoreByInterval(Map<Integer, Integer> scoreByInterval) {
    this.scoreByInterval = scoreByInterval;
  }

  public float getCurrentPrice() {
    return currentPrice;
  }

  public void setCurrentPrice(float currentPrice) {
    this.currentPrice = currentPrice;
  }
}
