package tradingbot.model;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Relationship;


import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created by truongnhukhang on 6/8/18.
 */
@NodeEntity
public class Candle {
  @Id
  @GeneratedValue
  Long id;
  String symbol;
  long openTime;
  long closeTime;
  int interval;
  boolean bullish;
  float open;
  float close;
  float highest;
  float lowest;
  float volume;
  float quoteVolume;
  Set<Signal> signals;
  long next;
  long previous;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public long getOpenTime() {
    return openTime;
  }

  public void setOpenTime(long openTime) {
    this.openTime = openTime;
  }

  public long getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(long closeTime) {
    this.closeTime = closeTime;
  }

  public float getOpen() {
    return open;
  }

  public void setOpen(float open) {
    this.open = open;
  }

  public float getClose() {
    return close;
  }

  public void setClose(float close) {
    this.close = close;
  }

  public float getHighest() {
    return highest;
  }

  public void setHighest(float highest) {
    this.highest = highest;
  }

  public float getLowest() {
    return lowest;
  }

  public void setLowest(float lowest) {
    this.lowest = lowest;
  }

  public float getVolume() {
    return volume;
  }

  public void setVolume(float volume) {
    this.volume = volume;
  }

  public float getQuoteVolume() {
    return quoteVolume;
  }

  public void setQuoteVolume(float quoteVolume) {
    this.quoteVolume = quoteVolume;
  }

  public Set<Signal> getSignals() {
    return signals;
  }

  public void setSignals(Set<Signal> signals) {
    this.signals = signals;
  }

  public long getNext() {
    return next;
  }

  public void setNext(long next) {
    this.next = next;
  }

  public long getPrevious() {
    return previous;
  }

  public void setPrevious(long previous) {
    this.previous = previous;
  }

  public int getInterval() {
    return interval;
  }

  public void setInterval(int interval) {
    this.interval = interval;
  }

  public boolean isBullish() {
    return bullish;
  }

  public void setBullish(boolean bullish) {
    this.bullish = bullish;
  }


  public Candle copyWithoutSignal() {
    this.id = null;
    this.signals = null;
    return this;
  }

  public Candle() {
  }
}
