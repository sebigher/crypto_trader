package tradingbot.reader;

import tradingbot.model.Candle;

import java.util.List;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public interface ExchangeReader {
    List<Candle> readCandleFromExchange(String code, int candleTime, long startTime, long endTime);
}
