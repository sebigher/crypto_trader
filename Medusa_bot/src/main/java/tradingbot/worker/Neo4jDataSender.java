package tradingbot.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tradingbot.model.Candle;
import tradingbot.model.GroupCandle;
import tradingbot.model.Signal;
import tradingbot.repository.CandleRepository;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.SignalRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class Neo4jDataSender implements DataSender {

  @Autowired
  CandleRepository candleRepository;

  @Autowired
  SignalRepository signalRepository;

  @Autowired
  GroupCandleRepository groupCandleRepository;

  @Override
  @Transactional
  public void sendCandles(List<Candle> candles) {
    if(candles.size()>0) {
      List<Candle> insertCandles = new ArrayList<>();
      List<Signal> signalList = new ArrayList<>();
      signalRepository.findAll().forEach(signal -> signalList.add(signal));
      List<Candle> existedCandles = candleRepository
          .findCandlesBySymbolInAndCloseTimeInAndIntervalEquals(candles.stream().map(Candle::getSymbol).collect(Collectors.toList())
              ,candles.stream().map(Candle::getCloseTime).collect(Collectors.toList()),candles.get(0).getInterval());
      for (int i = 0; i < candles.size(); i++) {
        Candle candle = candles.get(i);
        if(candle.getSignals()!=null) {
          List<Signal> signalInCandle = signalList.stream().filter(signal -> candle.getSignals().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
          candle.setSignals(new HashSet<>(signalInCandle));
        }
        Optional<Candle> candleExistedOptional = existedCandles.stream().filter(candleExist ->
            candleExist.getSymbol().equals(candle.getSymbol()) && candleExist.getCloseTime()==candle.getCloseTime()).findFirst();
        if(candleExistedOptional.isPresent()) {
          Candle candleExisted = candleExistedOptional.get();
          candleExisted.setSignals(candle.getSignals());
          insertCandles.add(candleExisted);
        } else {
          if(i<candles.size()-1) {
            candle.setNext(candles.get(i+1).getOpenTime());
          }
          if(i>1) {
            candle.setPrevious(candles.get(i-1).getOpenTime());
          }
          insertCandles.add(candle);
        }
      }
      boolean isRunning = true;
      try {
        while(isRunning) {
          Iterable<Candle> candleIterable = candleRepository.saveAll(insertCandles);
          if(candleIterable.iterator().hasNext()) {
            isRunning = false;
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
        try {
          Thread.sleep(500);
        } catch (InterruptedException e1) {
          e1.printStackTrace();
        }
      }
    }
  }

  @Override
  @Transactional
  public void sendGroupCandles(List<GroupCandle> groupCandles) {
    List<Signal> signalList = new ArrayList<>();
    List<String> symbols = groupCandles.stream().map(groupCandle -> groupCandle.getSymbol()).distinct().collect(Collectors.toList());
    List<Long> openTimes = groupCandles.stream().map(groupCandle -> groupCandle.getOpenTime()).distinct().collect(Collectors.toList());
    List<GroupCandle> groupCandleExisted = groupCandleRepository.findGroupCandlesBySymbolInAndAndOpenTimeIn(symbols,openTimes,2);
    List<GroupCandle> groupCandleSaveList = new ArrayList<>();
    List<GroupCandle> groupCandleNew = groupCandles.stream().filter(groupCandle -> groupCandleExisted.stream().noneMatch(groupCandle1 -> groupCandle1.getSymbol().equals(groupCandle.getSymbol()) && groupCandle1.getOpenTime()==groupCandle.getOpenTime())).collect(Collectors.toList());
    groupCandleSaveList.addAll(groupCandleNew);
    groupCandleSaveList.addAll(groupCandleExisted);
    signalRepository.findAll().forEach(signal -> signalList.add(signal));
    updateLatestSignals(groupCandles, signalList, groupCandleSaveList);
    groupCandleRepository.saveAll(groupCandleSaveList);
    System.out.println("Finish Save group candle");
  }

  private void updateLatestSignals(List<GroupCandle> groupCandles, List<Signal> signalList, List<GroupCandle> groupCandleSaveList) {
    for (int i = 0; i < groupCandleSaveList.size(); i++) {
      GroupCandle groupCandle = groupCandleSaveList.get(i);
      GroupCandle groupCandleNew = groupCandles.stream().filter(groupCandle1 -> groupCandle1.getSymbol().equals(groupCandle.getSymbol()) && groupCandle1.getOpenTime() == groupCandle.getOpenTime()).findFirst().get();
      groupCandle.setBtcMA5_4h(groupCandleNew.getBtcMA5_4h());
      groupCandle.setBtcMA5_1d(groupCandleNew.getBtcMA5_1d());
      if(groupCandleNew.getSignals30min()!=null) {
        List<Signal> signalInCandle_30min = signalList.stream().filter(signal -> groupCandleNew.getSignals30min().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
        groupCandle.setSignals30min(new HashSet<>(signalInCandle_30min));
      } else {
        groupCandle.setSignals30min(null);
      }
      if(groupCandleNew.getSignals60min()!=null) {
        List<Signal> signalInCandle_60min = signalList.stream().filter(signal -> groupCandleNew.getSignals60min().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
        groupCandle.setSignals60min(new HashSet<>(signalInCandle_60min));
      } else {
        groupCandle.setSignals60min(null);
      }

      if(groupCandleNew.getSignals120min()!=null) {
        List<Signal> signalInCandle_120min = signalList.stream().filter(signal -> groupCandleNew.getSignals120min().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
        groupCandle.setSignals120min(new HashSet<>(signalInCandle_120min));
      } else {
        groupCandle.setSignals120min(null);
      }

      if(groupCandleNew.getSignals240min()!=null) {
        List<Signal> signalInCandle_240min = signalList.stream().filter(signal -> groupCandleNew.getSignals240min().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
        groupCandle.setSignals240min(new HashSet<>(signalInCandle_240min));
      } else {
        groupCandle.setSignals240min(null);
      }

      if(groupCandleNew.getSignals1440min()!=null) {
        List<Signal> signalInCandle_1440min = signalList.stream().filter(signal -> groupCandleNew.getSignals1440min().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
        groupCandle.setSignals1440min(new HashSet<>(signalInCandle_1440min));
      } else {
        groupCandle.setSignals1440min(null);
      }

    }
  }
}
