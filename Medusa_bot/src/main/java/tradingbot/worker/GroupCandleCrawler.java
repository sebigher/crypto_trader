package tradingbot.worker;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.SymbolInfo;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import tradingbot.model.*;
import tradingbot.reader.BinanceBackTestReader;
import tradingbot.reader.ExchangeReader;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.SignalRepository;
import tradingbot.service.GroupCandleService;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by truongnhukhang on 6/25/18.
 */
@Component
public class GroupCandleCrawler {
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient client = factory.newRestClient();
  @Autowired
  DataSender neo4jDataSender;
  Map<Long, Float> btcPriceCache = new ConcurrentHashMap<>();
  Map<String, List<Float>> btcMA5Cache = new ConcurrentHashMap<>();
  @Autowired
  GroupCandleRepository groupCandleRepository;

  @Autowired
  GroupCandleService groupCandleService;

  @Autowired
  SignalRepository signalRepository;

  public void backupToJson() throws IOException {
    System.out.println("Start : " + new Date());
    long totalPage = 88;
    List<GroupCandle> groupCandleList = new ArrayList<>();
    for (int i = 80; i < totalPage ; i++) {
      Page<GroupCandle> groupCandlePage = groupCandleRepository.findAll(PageRequest.of(i,10000),2);
      groupCandleList.addAll(groupCandlePage.getContent());
    }
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.writerWithDefaultPrettyPrinter().writeValue(new java.io.File("/Users/truongnhukhang/work/test-project/data_2.json"),groupCandleList);
    System.out.println("End : " + new Date());
  }

  public void saveDataFromJson() throws IOException {
    System.out.println("Start : " + new Date());
    ObjectMapper objectMapper = new ObjectMapper();
    List<GroupCandle> groupCandles = new ArrayList<>();
    groupCandles.addAll(objectMapper.readValue(new File("C:\\Users\\khangtn\\Downloads\\Archive\\data.json"),new TypeReference<List<GroupCandle>>(){}));
    groupCandles.addAll(objectMapper.readValue(new File("C:\\Users\\khangtn\\Downloads\\Archive\\data_2.json"),new TypeReference<List<GroupCandle>>(){}));
    List<Signal> signalList = new ArrayList<>();
    System.out.println("read file done : " + new Date());
    signalRepository.findAll().forEach(signal -> signalList.add(signal));
    List<GroupCandle> groupCandleNews = groupCandles.stream().map(groupCandle -> {
      GroupCandle temp = new GroupCandle();
      temp.setSymbol(groupCandle.getSymbol());
      temp.setOpenTime(groupCandle.getOpenTime());
      if(groupCandle.getInterval30Min().getSignals()!=null) {
        temp.setSignals30min(new HashSet<>(convertRawSignalToSignalInDb(signalList, groupCandle.getInterval30Min())));
      } else {
        temp.setSignals30min(null);
      }
      if(groupCandle.getInterval60Min().getSignals()!=null) {
        temp.setSignals60min(new HashSet<>(convertRawSignalToSignalInDb(signalList, groupCandle.getInterval60Min())));
      } else {
        temp.setSignals60min(null);
      }
      if(groupCandle.getInterval120Min().getSignals()!=null) {
        temp.setSignals120min(new HashSet<>(convertRawSignalToSignalInDb(signalList, groupCandle.getInterval120Min())));
      } else {
        temp.setSignals120min(null);
      }
      if(groupCandle.getInterval240Min().getSignals()!=null) {
        temp.setSignals240min(new HashSet<>(convertRawSignalToSignalInDb(signalList, groupCandle.getInterval240Min())));
      } else {
        temp.setSignals240min(null);
      }
      if(groupCandle.getInterval1440Min().getSignals()!=null) {
        temp.setSignals1440min(new HashSet<>(convertRawSignalToSignalInDb(signalList, groupCandle.getInterval1440Min())));
      } else {
        temp.setSignals1440min(null);
      }
      temp.setInterval30Min(groupCandle.getInterval30Min().copyWithoutSignal());
      temp.setInterval60Min(groupCandle.getInterval60Min().copyWithoutSignal());
      temp.setInterval120Min(groupCandle.getInterval120Min().copyWithoutSignal());
      temp.setInterval240Min(groupCandle.getInterval240Min().copyWithoutSignal());
      temp.setInterval1440Min(groupCandle.getInterval1440Min().copyWithoutSignal());
      if(groupCandle.getSymbol().startsWith("BTCUSDT")) {
        temp.setBtcMA5_1d(groupCandle.getBtcMA5_1d());
        temp.setBtcMA5_4h(groupCandle.getBtcMA5_4h());
      }
      temp.setPriceAfter5min(groupCandle.getPriceAfter5min());
      temp.setPriceAfter10min(groupCandle.getPriceAfter10min());
      temp.setPriceAfter15min(groupCandle.getPriceAfter15min());
      temp.setPriceAfter30min(groupCandle.getPriceAfter30min());
      temp.setPriceAfter60min(groupCandle.getPriceAfter60min());
      temp.setPriceAfter120min(groupCandle.getPriceAfter120min());
      temp.setPriceAfter240min(groupCandle.getPriceAfter240min());
      temp.setPriceAfter480min(groupCandle.getPriceAfter480min());
      temp.setPriceAfter1440min(groupCandle.getPriceAfter1440min());
      temp.setPriceAfter2480min(groupCandle.getPriceAfter2480min());
      return temp;
    }).collect(Collectors.toList());
    System.out.println("Convert done : " + new Date());
    System.out.println("Start saving ");
    List<List<GroupCandle>> lists = ListUtils.partition(groupCandleNews,10000);
    final int[] count = {lists.size()};
    lists.forEach(list -> {
      groupCandleRepository.saveAll(list);
      count[0]--;
      System.out.println("Need to process : " + count[0]*10000);
    });
    System.out.println("Save done : " + new Date());
  }

  private List<Signal> convertRawSignalToSignalInDb(List<Signal> signalList, Candle candle) {
    return signalList.stream().filter(signal -> candle.getSignals().stream().anyMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
  }

  @Async
  public void crawAllSymbol(long startTime, long endTime, boolean ignoreExistedData, boolean ignoreFuturePrice) {
    ExecutorService executor = Executors.newFixedThreadPool(5);
    List<Callable<GroupCandle>> callables = new ArrayList<>();
    try {
      List<SymbolInfo> symbols = client.getExchangeInfo().getSymbols();
      System.out.println("FULL COIN SIZE : " + symbols.size());
      symbols = symbols.stream().filter(symbolInfo -> symbolInfo.getSymbol().endsWith("BTC") || symbolInfo.getSymbol().endsWith("BTCUSDT")).collect(Collectors.toList());
      for (int index = 0; index < symbols.size(); index++) {
        long startTimeCraw = System.currentTimeMillis();
        System.out.println((symbols.size() - index) + " Coins need to process");
        SymbolInfo symbolInfo = symbols.get(index);
        String symbol = symbolInfo.getSymbol();
        List<GroupCandle> groupCandles = new ArrayList<>();
        TimeSeries timeSeries_30min = new BaseTimeSeries();
        TAWorker taWorker_30min = new TAWorker(timeSeries_30min);
        ExchangeReader exchangeReader_30min = new BinanceBackTestReader(timeSeries_30min);
        List<Candle> candles_30min = exchangeReader_30min.readCandleFromExchange(symbol, 30, startTime, endTime);
        if (candles_30min.size() > 0) {
          if (ignoreExistedData) {
            List<String> symbolGroups = new ArrayList<>();
            List<Long> opentimeGroups = new ArrayList<>();
            symbolGroups.add(symbol);
            for (int i = 960; i <= timeSeries_30min.getEndIndex(); i++) {
              opentimeGroups.add(candles_30min.get(i).getOpenTime());
            }
            Long groupCandleCount = groupCandleRepository.countAllBySymbolInAndAndOpenTimeIn(symbolGroups, opentimeGroups, 1);
            if (groupCandleCount != null && groupCandleCount == candles_30min.size() - 960) {
              System.out.println(symbol + " : " + startTime + " - " + endTime + " Already in database ");
              return;
            }
          }
          TimeSeries timeSeries_60min = new BaseTimeSeries();
          TAWorker taWorker_60min = new TAWorker(timeSeries_30min);
          ExchangeReader exchangeReader_60min = new BinanceBackTestReader(timeSeries_60min);
          List<Candle> candles_60min = exchangeReader_60min.readCandleFromExchange(symbol, 60, startTime, endTime);

          TimeSeries timeSeries_120min = new BaseTimeSeries();
          TAWorker taWorker_120min = new TAWorker(timeSeries_120min);
          ExchangeReader exchangeReader_120min = new BinanceBackTestReader(timeSeries_120min);
          List<Candle> candles_120min = exchangeReader_120min.readCandleFromExchange(symbol, 120, startTime, endTime);

          TimeSeries timeSeries_240min = new BaseTimeSeries();
          TAWorker taWorker_240min = new TAWorker(timeSeries_240min);
          ExchangeReader exchangeReader_240min = new BinanceBackTestReader(timeSeries_240min);
          List<Candle> candles_240min = exchangeReader_240min.readCandleFromExchange(symbol, 240, startTime, endTime);

          TimeSeries timeSeries_1440min = new BaseTimeSeries();
          TAWorker taWorker_1440min = new TAWorker(timeSeries_1440min);
          ExchangeReader exchangeReader_1440min = new BinanceBackTestReader(timeSeries_1440min);
          List<Candle> candles_1440min = exchangeReader_1440min.readCandleFromExchange(symbol, 1440, startTime, endTime);

          for (int i = 960; i <= timeSeries_30min.getEndIndex(); i++) {
            try {
              Candle candle_30min = candles_30min.get(i);
              GroupCandle groupCandle = new GroupCandle();
              groupCandle.setSymbol(symbol);
              groupCandle.setOpenTime(candle_30min.getOpenTime());
              groupCandle.setSignals30min(new HashSet<>(taWorker_30min.buildSignalListFromSeriesIndex(i)));
              if (symbol.startsWith("BTCUSDT")) {
                for (int j = 0; j < timeSeries_240min.getEndIndex(); j++) {
                  if (timeSeries_240min.getBar(j).getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && timeSeries_240min.getBar(j).getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime())
                    groupCandle.setBtcMA5_4h(taWorker_240min.buildListRecentMA5(j, 15));
                }
                for (int j = 0; j < timeSeries_1440min.getEndIndex(); j++) {
                  if (timeSeries_1440min.getBar(j).getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && timeSeries_1440min.getBar(j).getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime())
                    groupCandle.setBtcMA5_1d(taWorker_240min.buildListRecentMA5(j, 15));
                }
              }
              groupCandle.setInterval30Min(candle_30min);

              CandleBar candleBar_60min = (CandleBar) timeSeries_60min.getBarData()
                  .stream().filter(bar -> bar.getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && bar.getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime()).findFirst().get();
              candleBar_60min.setClosePrice(timeSeries_30min.getBar(i).getClosePrice());
              Candle candle_60min = candles_60min.stream().filter(candle -> candle.getOpenTime() <= candle_30min.getOpenTime()
                  && candle.getCloseTime() >= candle_30min.getCloseTime()).findFirst().get();
              groupCandle.setSignals60min(new HashSet<>(taWorker_60min.buildSignalListFromSeriesIndex(timeSeries_60min.getBarData().indexOf(candleBar_60min))));
              groupCandle.setInterval60Min(candle_60min);

              CandleBar candleBar_120min = (CandleBar) timeSeries_120min.getBarData()
                  .stream().filter(bar -> bar.getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && bar.getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime()).findFirst().get();
              candleBar_120min.setClosePrice(timeSeries_30min.getBar(i).getClosePrice());
              Candle candle_120min = candles_120min.stream().filter(candle -> candle.getOpenTime() <= candle_30min.getOpenTime()
                  && candle.getCloseTime() >= candle_30min.getCloseTime()).findFirst().get();
              groupCandle.setSignals120min(new HashSet<>(taWorker_120min.buildSignalListFromSeriesIndex(timeSeries_120min.getBarData().indexOf(candleBar_120min))));
              groupCandle.setInterval120Min(candle_120min);

              CandleBar candleBar_240min = (CandleBar) timeSeries_240min.getBarData()
                  .stream().filter(bar -> bar.getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && bar.getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime()).findFirst().get();
              candleBar_240min.setClosePrice(timeSeries_30min.getBar(i).getClosePrice());
              Candle candle_240min = candles_240min.stream().filter(candle -> candle.getOpenTime() <= candle_30min.getOpenTime()
                  && candle.getCloseTime() >= candle_30min.getCloseTime()).findFirst().get();
              groupCandle.setSignals240min(new HashSet<>(taWorker_240min.buildSignalListFromSeriesIndex(timeSeries_240min.getBarData().indexOf(candleBar_240min))));
              groupCandle.setInterval240Min(candle_240min);

              CandleBar candleBar_1440min = (CandleBar) timeSeries_1440min.getBarData()
                  .stream().filter(bar -> bar.getBeginTime().toInstant().toEpochMilli() <= candle_30min.getOpenTime()
                      && bar.getEndTime().toInstant().toEpochMilli() >= candle_30min.getCloseTime()).findFirst().get();
              candleBar_1440min.setClosePrice(timeSeries_30min.getBar(i).getClosePrice());
              Candle candle_1440min = candles_1440min.stream().filter(candle -> candle.getOpenTime() <= candle_30min.getOpenTime()
                  && candle.getCloseTime() >= candle_30min.getCloseTime()).findFirst().get();
              groupCandle.setSignals1440min(new HashSet<>(taWorker_1440min.buildSignalListFromSeriesIndex(timeSeries_1440min.getBarData().indexOf(candleBar_1440min))));
              groupCandle.setInterval1440Min(candle_1440min);
              if (!symbol.endsWith("BTCUSDT")) {
                createFutureJobsIfNeeded(ignoreFuturePrice, groupCandle,callables);
              }
              groupCandles.add(groupCandle);
            } catch (Exception e) {
              System.out.println("Number error : " + i);
              System.out.println("Time : " + candles_30min.get(i).getOpenTime() + " _ " + candles_30min.get(i).getCloseTime());
              e.printStackTrace();
              continue;
            }

          }
          if (!ignoreFuturePrice) {
            startExecuteFutureJobs(callables,executor);
          }

          neo4jDataSender.sendGroupCandles(groupCandles);
          callables.clear();
        }
        long endTimeCraw = System.currentTimeMillis();
        System.out.println("Time to craw " + symbol + " ~ " + ((endTimeCraw - startTimeCraw) / 1000 / 60) + " Minutes");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }


  }

  @Async
  public void crawFuturePrice() {
    ExecutorService executor = Executors.newFixedThreadPool(5);
    System.out.println("Start get GroupCandles with future prices = 0 : " + new Date());
    List<GroupCandle> groupCandles = groupCandleService.findGroupCandleWithFuturePriceEqualZero();
    System.out.println("Found : " + groupCandles.size() + " GroupCandles");
    List<Callable<GroupCandle>> callables = new ArrayList<>();
    groupCandles.forEach(groupCandle -> {
      Callable<GroupCandle> groupCandleCallable = () -> {
        buildFuturePrices(groupCandle);
        return groupCandle;
      };
      callables.add(groupCandleCallable);
    });
    try {
      System.out.println("Sleep Before Call API to avoid rate limit : " + new Date());
      Thread.sleep(60000);
    } catch (InterruptedException e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }

    int flag = 0;
    for (int i = 1; i < callables.size(); i++) {
      if (i % 1200 == 0) {
        try {
          System.out.println("Start call : " + new Date());
          List<GroupCandle> tempGroup = new ArrayList<>();
          List<Future<GroupCandle>> futures = executor.invokeAll(callables.subList(flag, i-1));
          futures.parallelStream().forEach(future -> {
            try {
              tempGroup.add(future.get());
            } catch (InterruptedException e) {
              e.printStackTrace();
            } catch (ExecutionException e) {
              e.printStackTrace();
            }
          });
          System.out.println("Start saving : ");
          groupCandleRepository.saveAll(tempGroup);
          System.out.println("Sleep Before Call API to avoid rate limit, " + ((callables.size() - i -1)) + " request need to call : " + new Date());
          Thread.sleep(60000);
        } catch (InterruptedException e) {
          System.out.println(e.getMessage());
          e.printStackTrace();
        }
        flag = i;
      }
    }
    if (flag < callables.size()) {
      List<Future<GroupCandle>> futures = null;
      System.out.println("Get last requests (" + ((callables.size() - flag)) + ")" + new Date());
      try {
        System.out.println("Start call : " + new Date());
        List<GroupCandle> tempGroup = new ArrayList<>();
        futures = executor.invokeAll(callables.subList(flag, callables.size() - 1));
        futures.parallelStream().forEach(future -> {
          try {
            tempGroup.add(future.get());
          } catch (InterruptedException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
          } catch (ExecutionException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
          }
        });
        System.out.println("Start saving : ");
        groupCandleRepository.saveAll(tempGroup);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.out.println("Finish task : " + new Date());
  }

  private void createFutureJobsIfNeeded(boolean ignoreFuturePrice, GroupCandle groupCandle,List<Callable<GroupCandle>> callables) {
    Callable<GroupCandle> groupCandleCallable = new Callable<GroupCandle>() {
      @Override
      public GroupCandle call() throws Exception {
        if (!ignoreFuturePrice) {
          buildFuturePrices(groupCandle);
        }

        return groupCandle;
      }
    };
    callables.add(groupCandleCallable);
  }

  private void startExecuteFutureJobs(List<Callable<GroupCandle>> callables,ExecutorService executor) {
    try {
      System.out.println("Sleep Before Call API to avoid rate limit : " + new Date());
      Thread.sleep(60000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    int flag = 0;
    for (int i = 0; i < callables.size(); i++) {
      if (i != 0 && i % 1200 == 0) {
        try {
          System.out.println("Start call : " + new Date());
          List<Future<GroupCandle>> futures = executor.invokeAll(callables.subList(flag, i));
          futures.parallelStream().forEach(future -> {
            try {
              future.get();
            } catch (InterruptedException e) {
              e.printStackTrace();
            } catch (ExecutionException e) {
              e.printStackTrace();
            }
          });
          System.out.println("Sleep Before Call API to avoid rate limit, " + ((callables.size() - i)) + " request need to call : " + new Date());
          Thread.sleep(60000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        flag = i;
      }
    }
    if (flag < callables.size() - 1) {
      List<Future<GroupCandle>> futures = null;
      System.out.println("Get last requests (" + ((callables.size() - flag)) + ")" + new Date());
      try {
        System.out.println("Start call : " + new Date());
        futures = executor.invokeAll(callables.subList(flag, callables.size() - 1));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      futures.parallelStream().forEach(future -> {
        try {
          future.get();
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
      });
    }
  }

//  private void buildBTCPrice(GroupCandle groupCandle) {
//    Date dateTime = new Date(groupCandle.getOpenTime());
//    long time5minAgo = dateTime.toInstant().minus(5, ChronoUnit.MINUTES).toEpochMilli();
//    List<Candlestick> candlesticks = null;
//    if (btcPriceCache.get(time5minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time5minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice5mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time5minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice5mAgo(btcPriceCache.get(time5minAgo));
//    }
//
//    long time10minAgo = dateTime.toInstant().minus(10, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time10minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time10minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice10mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time10minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice10mAgo(btcPriceCache.get(time10minAgo));
//    }
//
//    long time15minAgo = dateTime.toInstant().minus(15, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time15minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time15minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice15mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time15minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice15mAgo(btcPriceCache.get(time15minAgo));
//    }
//
//    long time30minAgo = dateTime.toInstant().minus(30, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time30minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time30minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice30mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time30minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice30mAgo(btcPriceCache.get(time30minAgo));
//    }
//
//    long time60minAgo = dateTime.toInstant().minus(60, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time60minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time60minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice60mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time60minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice60mAgo(btcPriceCache.get(time60minAgo));
//    }
//
//    long time120minAgo = dateTime.toInstant().minus(120, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time120minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time120minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice120mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time120minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice120mAgo(btcPriceCache.get(time120minAgo));
//    }
//
//    long time240minAgo = dateTime.toInstant().minus(240, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time240minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time240minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice240mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time240minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice240mAgo(btcPriceCache.get(time240minAgo));
//    }
//
//    long time480minAgo = dateTime.toInstant().minus(480, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time480minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time480minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice480mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time480minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice480mAgo(btcPriceCache.get(time480minAgo));
//    }
//
//    long time1440minAgo = dateTime.toInstant().minus(1440, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time1440minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time1440minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice1440mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time1440minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice1440mAgo(btcPriceCache.get(time1440minAgo));
//    }
//
//    long time2480minAgo = dateTime.toInstant().minus(2480, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time2480minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time2480minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice2480mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time2480minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice2480mAgo(btcPriceCache.get(time2480minAgo));
//    }
//
//    long time10080minAgo = dateTime.toInstant().minus(10080, ChronoUnit.MINUTES).toEpochMilli();
//    if (btcPriceCache.get(time10080minAgo) == null) {
//      candlesticks = client.getCandlestickBars("BTCUSDT", CandlestickInterval.ONE_MINUTE, 1, time10080minAgo, null);
//      if (candlesticks != null && candlesticks.size() > 0) {
//        groupCandle.setBtcPrice10080mAgo(Float.parseFloat(candlesticks.get(0).getClose()));
//        btcPriceCache.put(time10080minAgo, Float.parseFloat(candlesticks.get(0).getClose()));
//      }
//    } else {
//      groupCandle.setBtcPrice10080mAgo(btcPriceCache.get(time10080minAgo));
//    }
//  }

  private void buildMA5BTC(GroupCandle groupCandle) {
    if (btcMA5Cache.get("240" + "_" + groupCandle.getOpenTime()) != null) {
      groupCandle.setBtcMA5_4h(btcMA5Cache.get("240" + "_" + groupCandle.getOpenTime()));
    } else {
      List<Candlestick> candlesticks4h = client.getCandlestickBars("BTCUSDT", CandlestickInterval.FOUR_HOURLY, 19, null, groupCandle.getOpenTime());
      TimeSeries timeSeries4h = new BaseTimeSeries();
      candlesticks4h.forEach(candlestick -> {
        CandleBar bar = new CandleBar(Duration.ofHours(4),
            Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
            Decimal.valueOf(candlestick.getOpen()),
            Decimal.valueOf(candlestick.getHigh()),
            Decimal.valueOf(candlestick.getLow()),
            Decimal.valueOf(candlestick.getClose()),
            Decimal.valueOf(candlestick.getVolume()));
        try {
          timeSeries4h.addBar(bar);
        } catch (IllegalArgumentException e) {
          System.out.println(timeSeries4h.getLastBar().getBeginTime() + " _ " + timeSeries4h.getLastBar().getEndTime());
          System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
        }
      });
      SMAIndicator ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries4h), 5);
      List<Float> ma5Value = new ArrayList<>();
      for (int i = 5; i < timeSeries4h.getEndIndex(); i++) {
        ma5Value.add(ma5Indicator.getValue(i).floatValue());
      }
      btcMA5Cache.put("240" + "_" + groupCandle.getOpenTime(), ma5Value);
      groupCandle.setBtcMA5_4h(ma5Value);
    }
    if (btcMA5Cache.get("1440" + "_" + groupCandle.getOpenTime()) != null) {
      groupCandle.setBtcMA5_1d(btcMA5Cache.get("1440" + "_" + groupCandle.getOpenTime()));

    } else {

      List<Candlestick> candlesticks1d = client.getCandlestickBars("BTCUSDT", CandlestickInterval.DAILY, 19, null, groupCandle.getOpenTime());
      TimeSeries timeSeries1d = new BaseTimeSeries();
      candlesticks1d.forEach(candlestick -> {
        CandleBar bar = new CandleBar(Duration.ofHours(4),
            Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
            Decimal.valueOf(candlestick.getOpen()),
            Decimal.valueOf(candlestick.getHigh()),
            Decimal.valueOf(candlestick.getLow()),
            Decimal.valueOf(candlestick.getClose()),
            Decimal.valueOf(candlestick.getVolume()));
        try {
          timeSeries1d.addBar(bar);
        } catch (IllegalArgumentException e) {
          System.out.println(timeSeries1d.getLastBar().getBeginTime() + " _ " + timeSeries1d.getLastBar().getEndTime());
          System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
        }
      });
      SMAIndicator ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries1d), 5);
      List<Float> ma5Value1d = new ArrayList<>();
      for (int i = 5; i < timeSeries1d.getEndIndex(); i++) {
        ma5Value1d.add(ma5Indicator.getValue(i).floatValue());
      }
      btcMA5Cache.put("1440" + "_" + groupCandle.getOpenTime(), ma5Value1d);
      groupCandle.setBtcMA5_1d(ma5Value1d);


    }

  }

  private void buildFuturePrices(GroupCandle groupCandle) {
    Date candleOpenTime = new Date(groupCandle.getOpenTime());
    List<Candlestick> candlesticks = client.getCandlestickBars(groupCandle.getSymbol(), CandlestickInterval.FIVE_MINUTES, null, groupCandle.getOpenTime(), candleOpenTime.toInstant().plus(2, ChronoUnit.DAYS).toEpochMilli());
    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(5, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(5, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter5min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(10, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(10, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter10min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(15, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(15, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter15min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(30, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(30, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter30min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(60, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(60, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter60min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(120, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(120, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter120min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(240, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(240, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter240min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(480, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(480, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter480min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(1440, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(1440, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter1440min(Float.parseFloat(candlestick.getClose())));

    candlesticks.stream()
        .filter(candlestick -> candlestick.getOpenTime() >= candleOpenTime.toInstant().plus(2480, ChronoUnit.MINUTES).toEpochMilli()
            && candleOpenTime.toInstant().plus(2480, ChronoUnit.MINUTES).toEpochMilli() <= candlestick.getCloseTime())
        .findFirst().ifPresent(candlestick -> groupCandle.setPriceAfter2480min(Float.parseFloat(candlestick.getClose())));


  }
}
