package tradingbot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingbot.SignalType;
import tradingbot.converter.Converter;
import tradingbot.dto.GroupCandleDto;
import tradingbot.dto.PredictionDto;
import tradingbot.model.GroupCandle;
import tradingbot.model.GroupCandleFilterCondition;
import tradingbot.model.Signal;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.GroupCandleSearchRepository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PredictionServiceImpl implements PredictionService{

  @Autowired
  GroupCandleSearchRepository groupCandleSearchRepository;

  @Autowired
  GroupCandleRepository groupCandleRepository;

  @Override
  public PredictionDto buildPredictionFrom(GroupCandleFilterCondition groupCandleFilterCondition, Integer limit , Integer skip) {
    PredictionDto predictionDto = new PredictionDto();
    List<String> signal_30min = groupCandleFilterCondition.getSignalListByInterval().get(30);
    List<String> signal_60min = groupCandleFilterCondition.getSignalListByInterval().get(60);
    List<String> signal_120min = groupCandleFilterCondition.getSignalListByInterval().get(120);
    List<String> signal_240min = groupCandleFilterCondition.getSignalListByInterval().get(240);
    List<String> signal_1440min = groupCandleFilterCondition.getSignalListByInterval().get(1440);
    Integer score_30min = groupCandleFilterCondition.getScoreByInterval().get(30);
    Integer score_60min = groupCandleFilterCondition.getScoreByInterval().get(60);
    Integer score_120min = groupCandleFilterCondition.getScoreByInterval().get(120);
    Integer score_240min = groupCandleFilterCondition.getScoreByInterval().get(240);
    Integer score_1440min = groupCandleFilterCondition.getScoreByInterval().get(1440);
    Long opentime = groupCandleFilterCondition.getOpentime()!=null ? groupCandleFilterCondition.getOpentime() : Long.valueOf("1529625600000");
    List<GroupCandleDto> groupCandleDtos = new ArrayList<>();
    Date start = new Date();
    List<GroupCandle> groupCandles = groupCandleSearchRepository.
        findGroupCandlesBySimilarSignal("and",
            signal_30min,signal_60min,signal_120min,signal_240min,signal_1440min,
            score_30min,score_60min,score_120min,score_240min,score_1440min,20,10,opentime
        );
    Date end = new Date();
    System.out.println("Repository Time : " + (end.getTime()-start.getTime())/60000);
    if(groupCandles.size()>0) {
      List<Long> ids = groupCandles.stream().map(GroupCandle::getId).collect(Collectors.toList());
      List<Long> openTimes = groupCandles.stream().map(GroupCandle::getOpenTime).collect(Collectors.toList());
      Iterable<GroupCandle> groupCandleIterable = groupCandleRepository.findAllById(ids,1);
      Iterable<GroupCandle> btcGroupCandle = groupCandleRepository.findGroupCandlesBySymbolInAndAndOpenTimeIn(Arrays.asList("BTCUSDT"),openTimes,1);
      groupCandleIterable.forEach(groupCandle -> {
        groupCandleDtos.add(Converter.from(btcGroupCandle,groupCandle));
      });
      predictionDto.setGroupCandleDtos(groupCandleDtos);
      predictionDto.setCurrentPrice(groupCandleFilterCondition.getCurrentPrice());
      Integer totalScore = getScoreFromBullishSignal(groupCandleFilterCondition.getSignalListByInterval());
      groupCandles = new ArrayList<>();
      groupCandleIterable.forEach(groupCandles::add);
      float percentChangeAfter5min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(5, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter10min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(10, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter15min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(15, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter30min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(30, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter60min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(60, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter120min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(120, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter240min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(240, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter480min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(480, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter1440min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(1440, totalScore)).reduce(Float::sum).get();
      float percentChangeAfter2480min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(2480, totalScore)).reduce(Float::sum).get();
      predictionDto.setPriceAfter5min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter5min / 100);
      predictionDto.setPriceAfter10min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter10min / 100);
      predictionDto.setPriceAfter15min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter15min / 100);
      predictionDto.setPriceAfter30min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter30min / 100);
      predictionDto.setPriceAfter60min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter60min / 100);
      predictionDto.setPriceAfter120min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter120min / 100);
      predictionDto.setPriceAfter240min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter240min / 100);
      predictionDto.setPriceAfter480min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter480min / 100);
      predictionDto.setPriceAfter1440min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter1440min / 100);
      predictionDto.setPriceAfter2480min(predictionDto.getCurrentPrice() + predictionDto.getCurrentPrice() * percentChangeAfter2480min / 100);
      return predictionDto;
    }
    return null;
  }

  public Integer getScoreFromBullishSignal(Map<Integer, List<String>> bullishSignal) {
    int score_30 = 0;
    int score_60 = 0;
    int score_120 = 0;
    int score_240 = 0;
    int score_1440 = 0;
    if (bullishSignal.get(30) != null) {
      score_30 = bullishSignal.get(30).stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get(60) != null) {
      score_60 = bullishSignal.get(60).stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get(120) != null) {
      score_120 = bullishSignal.get(120).stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get(240) != null) {
      score_240 = bullishSignal.get(240).stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get(1440) != null) {
      score_1440 = bullishSignal.get(1440).stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    return score_30 + score_60 * 2 + score_120 * 3 + score_240 * 4 + score_1440 * 5;
  }

  private Function<String, Integer> mapSignalStringToTheirScore() {
    return s -> {
      return Stream.of(SignalType.values()).filter(signalType -> signalType.toString().equals(s)).findFirst().get().getScore();
    };
  }
}
