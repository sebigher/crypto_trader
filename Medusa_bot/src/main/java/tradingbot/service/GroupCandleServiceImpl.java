package tradingbot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingbot.converter.Converter;
import tradingbot.dto.GroupCandleDto;
import tradingbot.model.GroupCandle;
import tradingbot.model.GroupCandleFilterCondition;
import tradingbot.model.Signal;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.GroupCandleSearchRepository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class GroupCandleServiceImpl implements GroupCandleService {
  
  @Autowired
  GroupCandleSearchRepository groupCandleSearchRepository;

  @Autowired
  GroupCandleRepository groupCandleRepository;
  
  @Override
  public List<GroupCandleDto> findGroupCandleBySimilarSignals(GroupCandleFilterCondition groupCandleFilterCondition, Integer limit , Integer skip) {
    List<String> signal_30min = groupCandleFilterCondition.getSignalListByInterval().get(30);
    List<String> signal_60min = groupCandleFilterCondition.getSignalListByInterval().get(60);
    List<String> signal_120min = groupCandleFilterCondition.getSignalListByInterval().get(120);
    List<String> signal_240min = groupCandleFilterCondition.getSignalListByInterval().get(240);
    List<String> signal_1440min = groupCandleFilterCondition.getSignalListByInterval().get(1440);
    Integer score_30min = groupCandleFilterCondition.getScoreByInterval().get(30);
    Integer score_60min = groupCandleFilterCondition.getScoreByInterval().get(60);
    Integer score_120min = groupCandleFilterCondition.getScoreByInterval().get(120);
    Integer score_240min = groupCandleFilterCondition.getScoreByInterval().get(240);
    Integer score_1440min = groupCandleFilterCondition.getScoreByInterval().get(1440);
    Long opentime = groupCandleFilterCondition.getOpentime()!=null ? groupCandleFilterCondition.getOpentime() : Long.valueOf("1529625600000");
    List<GroupCandleDto> groupCandleDtos = new ArrayList<>();
    Date start = new Date();
    List<GroupCandle> groupCandles = groupCandleSearchRepository.
        findGroupCandlesBySimilarSignal("and",
            signal_30min,signal_60min,signal_120min,signal_240min,signal_1440min,
            score_30min,score_60min,score_120min,score_240min,score_1440min,20,10,opentime
        );
    Date end = new Date();
    System.out.println("Repository Time : " + (end.getTime()-start.getTime())/60000);
    List<Long> ids = groupCandles.stream().map(GroupCandle::getId).collect(Collectors.toList());
    List<Long> openTimes = groupCandles.stream().map(GroupCandle::getOpenTime).collect(Collectors.toList());
    Iterable<GroupCandle> groupCandleIterable = groupCandleRepository.findAllById(ids,1);
    Iterable<GroupCandle> btcGroupCandle = groupCandleRepository.findGroupCandlesBySymbolInAndAndOpenTimeIn(Arrays.asList("BTCUSDT"),openTimes,1);
    groupCandleIterable.forEach(groupCandle -> {
      groupCandleDtos.add(Converter.from(btcGroupCandle,groupCandle));
    });
    return groupCandleDtos;
  }

  @Override
  public GroupCandleDto findGroupCandleBySymbolAndOpentime(String symbol, long opentime) {
    GroupCandle groupCandle = groupCandleRepository.findGroupCandleBySymbolAndOpenTime(symbol,opentime,2);
    GroupCandleDto groupCandleDto = new GroupCandleDto();
    groupCandleDto.setSymbol(groupCandle.getSymbol());
    Map<String,List<String>> bullishSignal = new ConcurrentHashMap<>();
    bullishSignal.put("30",groupCandle.getInterval30Min().getSignals()==null ? new ArrayList<>() : groupCandle.getInterval30Min().getSignals().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("60",groupCandle.getInterval60Min().getSignals()==null ? new ArrayList<>() : groupCandle.getInterval60Min().getSignals().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("120",groupCandle.getInterval120Min().getSignals()==null ? new ArrayList<>() : groupCandle.getInterval120Min().getSignals().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("240",groupCandle.getInterval240Min().getSignals()==null ? new ArrayList<>() : groupCandle.getInterval240Min().getSignals().stream().map(Signal::getType).collect(Collectors.toList()));
    bullishSignal.put("1440",groupCandle.getInterval1440Min().getSignals()==null ? new ArrayList<>() : groupCandle.getInterval1440Min().getSignals().stream().map(Signal::getType).collect(Collectors.toList()));
    groupCandleDto.setBullishSignal(bullishSignal);
    groupCandleDto.setCloseTime(groupCandle.getInterval30Min().getCloseTime());
    groupCandleDto.setPrice(groupCandle.getInterval30Min().getClose());
    return groupCandleDto;
  }

  @Override
  public List<GroupCandle> findGroupCandleWithFuturePriceEqualZero() {
    Iterable<Long> groupCandleIds = groupCandleSearchRepository.findGroupCandleIdsWithFuturePriceEqualZero();
    List<GroupCandle> groupCandles = new ArrayList<>();
    int count = 1;
    List<Long> temp = new ArrayList<>();
    Iterator<Long> iterator = groupCandleIds.iterator();
    while (iterator.hasNext()) {
      temp.add(iterator.next());
      if(count%10000==0) {
        groupCandleRepository.findAllById(temp,2).forEach(groupCandle -> groupCandles.add(groupCandle));
        temp = new ArrayList<>();
      }
      count++;
    }
    if(temp.size()%10000!=0) {
      groupCandleRepository.findAllById(temp,2).forEach(groupCandle -> groupCandles.add(groupCandle));
    }
    return groupCandles;
  }
}
