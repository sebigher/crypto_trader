package tradingbot.dto;

import java.util.List;

public class PredictionDto {
  List<GroupCandleDto> groupCandleDtos;
  String algorithm = "basic";
  float currentPrice;
  float priceAfter5min;
  float priceAfter10min;
  float priceAfter15min;
  float priceAfter30min;
  float priceAfter60min;
  float priceAfter120min;
  float priceAfter240min;
  float priceAfter480min;
  float priceAfter1440min;
  float priceAfter2480min;

  public List<GroupCandleDto> getGroupCandleDtos() {
    return groupCandleDtos;
  }

  public void setGroupCandleDtos(List<GroupCandleDto> groupCandleDtos) {
    this.groupCandleDtos = groupCandleDtos;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }

  public float getCurrentPrice() {
    return currentPrice;
  }

  public void setCurrentPrice(float currentPrice) {
    this.currentPrice = currentPrice;
  }

  public float getPriceAfter5min() {
    return priceAfter5min;
  }

  public void setPriceAfter5min(float priceAfter5min) {
    this.priceAfter5min = priceAfter5min;
  }

  public float getPriceAfter10min() {
    return priceAfter10min;
  }

  public void setPriceAfter10min(float priceAfter10min) {
    this.priceAfter10min = priceAfter10min;
  }

  public float getPriceAfter15min() {
    return priceAfter15min;
  }

  public void setPriceAfter15min(float priceAfter15min) {
    this.priceAfter15min = priceAfter15min;
  }

  public float getPriceAfter30min() {
    return priceAfter30min;
  }

  public void setPriceAfter30min(float priceAfter30min) {
    this.priceAfter30min = priceAfter30min;
  }

  public float getPriceAfter60min() {
    return priceAfter60min;
  }

  public void setPriceAfter60min(float priceAfter60min) {
    this.priceAfter60min = priceAfter60min;
  }

  public float getPriceAfter120min() {
    return priceAfter120min;
  }

  public void setPriceAfter120min(float priceAfter120min) {
    this.priceAfter120min = priceAfter120min;
  }

  public float getPriceAfter240min() {
    return priceAfter240min;
  }

  public void setPriceAfter240min(float priceAfter240min) {
    this.priceAfter240min = priceAfter240min;
  }

  public float getPriceAfter480min() {
    return priceAfter480min;
  }

  public void setPriceAfter480min(float priceAfter480min) {
    this.priceAfter480min = priceAfter480min;
  }

  public float getPriceAfter1440min() {
    return priceAfter1440min;
  }

  public void setPriceAfter1440min(float priceAfter1440min) {
    this.priceAfter1440min = priceAfter1440min;
  }

  public float getPriceAfter2480min() {
    return priceAfter2480min;
  }

  public void setPriceAfter2480min(float priceAfter2480min) {
    this.priceAfter2480min = priceAfter2480min;
  }
}
