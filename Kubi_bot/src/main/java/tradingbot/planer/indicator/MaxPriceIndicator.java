package tradingbot.planer.indicator;

import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;

/**
 * Created by khangtn on 3/25/2018.
 */
public class MaxPriceIndicator extends CachedIndicator<Decimal> {

  private TimeSeries series;

  public MaxPriceIndicator(TimeSeries series) {
    super(series);
    this.series = series;
  }

  @Override
  protected Decimal calculate(int index) {
    return series.getBar(index).getClosePrice();
  }
}
