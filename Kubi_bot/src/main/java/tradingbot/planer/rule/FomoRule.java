package tradingbot.planer.rule;

import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

/**
 * Created by khangtn on 3/24/2018.
 */
public class FomoRule extends AbstractRule {

  SMAIndicator volumeSMAIndicator_50;
  SMAIndicator volumeSMAIndicator_3;

  public FomoRule(SMAIndicator volumeSMAIndicator_50,SMAIndicator volumeSMAIndicator_3) {
    this.volumeSMAIndicator_50 = volumeSMAIndicator_50;
    this.volumeSMAIndicator_3 = volumeSMAIndicator_3;
  }

  @Override
  public boolean isSatisfied(int i, TradingRecord tradingRecord) {
    if(isThreeGreenCandleContinousOrHave4Bullish(i) && volumeSMAIndicator_3.getValue(i).isGreaterThan(volumeSMAIndicator_50.getValue(i).multipliedBy(3))) {
      return true;
    }
    return false;
  }

  public boolean isThreeGreenCandleContinousOrHave4Bullish(int i) {
    if(i > 2 && i < 4) {
      return isThreeBullishCandleContinous(i)
      ;
    } else if(i > 4){
      return isThreeBullishCandleContinous(i) || isHave4Bullish(i);
    }
    return false;
  }

  private boolean isThreeBullishCandleContinous(int i) {
    return volumeSMAIndicator_3.getTimeSeries().getBar(i).isBullish()
            && volumeSMAIndicator_3.getTimeSeries().getBar(i-1).isBullish()
            && volumeSMAIndicator_3.getTimeSeries().getBar(i-2).isBullish();
  }

  public boolean isHave4Bullish(int index) {
    int numberBullish = 0;
    for(int i = 0 ; i < 5;i++) {
      if(volumeSMAIndicator_3.getTimeSeries().getBar(index-i).isBullish()) {
        numberBullish = numberBullish + 1;
      }
    }
    if(numberBullish > 4) {
      return true;
    }
    return false;
  }
}
