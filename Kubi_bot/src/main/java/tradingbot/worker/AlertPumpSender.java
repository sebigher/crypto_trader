package tradingbot.worker;

import com.binance.api.client.domain.event.CandlestickEvent;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by truongnhukhang on 3/28/18.
 */
public class AlertPumpSender {
    Firestore db = null;
    CollectionReference bumpCollection;
    public AlertPumpSender() {
        InputStream serviceAccount = null;
        try {
            serviceAccount = AlertPumpSender.class.getResourceAsStream("/kubi-bot-key.json");
            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(credentials)
                    .build();
            FirebaseApp firebaseApp = FirebaseApp.initializeApp(options,"kubi_bot");
            db = FirestoreClient.getFirestore(firebaseApp);
            bumpCollection = db.collection("kubi_data");

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }

    public void send(String symbol,CandlestickEvent response,String volume,String quoteVolume ) {
        Map<String,Object> data = new HashMap<>();
        data.put("symbol",symbol);
        data.put("dateTime",response.getCloseTime());
        data.put("currentPrice",response.getClose());
        data.put("volume",volume);
        data.put("quoteVolume",quoteVolume);
        bumpCollection.add(data);
    }

    public void sendTest() {
        Map<String,Object> data = new HashMap<>();
        data.put("symbol","test");
        data.put("dateTime","125125");
        data.put("currentPrice","125125");
        data.put("volume","125125");
        data.put("quoteVolume","125125");
        bumpCollection.add(data);
    }

    public void deleteAll() throws ExecutionException, InterruptedException {
        int deleted = 0;
        // future.get() blocks on document retrieval
        ApiFuture<QuerySnapshot> future = bumpCollection.get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        while (documents.size()>0) {
            for (QueryDocumentSnapshot document : documents) {
                document.getReference().delete();
                ++deleted;
            }
            documents = future.get().getDocuments();
            System.out.println(deleted);
        }


    }
}
