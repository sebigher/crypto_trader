package tradingbot.reader;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public interface ExchangeReader {
    void readBarFromExchange(String code, int candleTime);
    void readBarFromJsonfile(String location);
}
