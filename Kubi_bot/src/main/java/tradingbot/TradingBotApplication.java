package tradingbot;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.SymbolInfo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.ta4j.core.*;
import tradingbot.reader.BinanceReader;
import tradingbot.reader.ExchangeReader;
import tradingbot.worker.AlertPumpSender;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;

@SpringBootApplication
public class TradingBotApplication implements CommandLineRunner {

	public static AlertPumpSender alertPumpSender;
	public static BlockingQueue<String> deadThread = new LinkedBlockingDeque<>();
	public static Map<String,ExchangeReader> exchangeReaderMap = new ConcurrentHashMap<>();

	public static void main(String[] args) {
		SpringApplication springApplication =
				new SpringApplicationBuilder()
						.sources(TradingBotApplication.class)
						.web(false)
						.build();

		springApplication.run(TradingBotApplication.class, args);


	}

	@Override
	public void run(String... strings) throws Exception {
		System.out.println("Start Kubi bot ");
		alertPumpSender = new AlertPumpSender();
		BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
		BinanceApiRestClient client = factory.newRestClient();
		List<String> symbols = client.getExchangeInfo().getSymbols().stream().map(SymbolInfo::getSymbol).collect(Collectors.toList());
		TimeSeries timeSeries = null;
		for (int i = 0; i < symbols.size(); i++) {
			if(symbols.get(i).endsWith("BTC")) {
				timeSeries = new BaseTimeSeries(symbols.get(i));
				timeSeries.setMaximumBarCount(100);
				ExchangeReader exchangeReader = new BinanceReader(timeSeries);
				exchangeReader.readBarFromExchange(symbols.get(i),1);
				exchangeReaderMap.put(symbols.get(i),exchangeReader);
				// sleep to avoid limit rate binance
				Thread.sleep(500);
			}

		}
		System.out.println("DeadThread pool : " + deadThread.size());
		System.out.println("Start DeadThread bot ");
		Thread recoverThread = new Thread(()->{
			boolean isRunning = true;
			while (isRunning) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(!deadThread.isEmpty()) {
					try {
						String symbol = deadThread.poll();
						System.out.println("Recover thread : " + symbol);
						ExchangeReader exchangeReader = exchangeReaderMap.get(symbol);
						exchangeReader.readBarFromExchange(symbol,1);
						exchangeReaderMap.put(symbol,exchangeReader);
					} catch (Exception e) {
						isRunning = false;
					}

				}
			}
		});
		recoverThread.start();
		System.out.println("Start close thread");
		Thread closeThread = new Thread(()->{
			boolean isRunning = true;
			while (isRunning) {
				try {
					Thread.sleep(60000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				exchangeReaderMap.keySet().forEach(key -> {
					BinanceReader binanceReader = (BinanceReader) exchangeReaderMap.get(key);
					binanceReader.closeSocketAfter6h();
				});

			}
		});
		closeThread.start();
	}
}
